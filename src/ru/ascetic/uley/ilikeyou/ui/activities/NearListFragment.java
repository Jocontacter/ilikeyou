﻿package ru.ascetic.uley.ilikeyou.ui.activities;

import java.sql.SQLException;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import ru.ascetic.uley.ilikeyou.ui.adapters.LazyNearAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class NearListFragment extends ListFragment implements PersonContactObserver
{
	public static final int TAB_ORDER = 1;

	private List<SocialUser> persons = null;
	private LazyNearAdapter adapter;

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// return super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.page_personlist, container, false);
	}

	public void onPersonReceived(SocialUser person, String event, int[] messageIDs, boolean is_like_received, boolean is_like_delivered)
	{
		if(event.equals("clear"))
		{
			fillListView();
			return;
		}

		if(event.equals("update"))
		{
			int pos = getPersonIndexByGid(person.gid);

			Integer lastKnownNum = null;

			if(pos < 0)
			{
				persons.add(0, person);
			}
			else
			{
				lastKnownNum = (Integer) persons.get(getPersonIndexByGid(person.gid)).tag;
				updatePerson(person);
			}

			int additionalNum = 0;
			if(lastKnownNum != null)
			{
				additionalNum = lastKnownNum;
			}

			person.tag = messageIDs.length + additionalNum;
		}
		else if(event.equals("add"))
		{
			// TODO: исправить эту ошибку! Такой ситуации быть не должно!
			if(person != null)
				persons.add(0, person);
		}

		adapter.notifyDataSetChanged();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		fillListView();
	}

	/** заменяет элемент в списке новым пользователем с идентичным gid'ом **/
	private void updatePerson(SocialUser person)
	{
		for(int i = 0; i < persons.size(); i++)
		{
			SocialUser user = persons.get(i);

			if(user.gid.equals(person.gid))
			{
				persons.remove(i);
				persons.add(0, person);
				return;
			}
		}
	}

	private int getPersonIndexByGid(String gid)
	{
		for(int i = 0; i < persons.size(); i++)
		{

			if(persons.get(i).gid.equals(gid))
			{
				return i;
			}
		}
		return -1;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);

		String gid = ((SocialUser) l.getItemAtPosition(position)).gid;

		persons.get(getPersonIndexByGid(gid)).tag = null;
		adapter.notifyDataSetChanged();

		Intent intent = new Intent(getActivity(), UserDetailsActivity.class);
		intent.putExtra("gid", gid);
		startActivity(intent);
	}

	private void fillListView()
	{
		try
		{
			persons = HelperFactory.getHelper().getSocialUserDAO().getNearUsers();
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			return;
		}

		adapter = new LazyNearAdapter(this.getActivity(), persons);
		getListView().setAdapter(adapter);
	}
}

package ru.ascetic.uley.ilikeyou.ui.activities;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.TutorialDialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

public enum Tutorials
{
	ENABLED("tutorial_enabled", R.string.tutorial_enabled),

	WHAT_NEW("tutorial_what_new", R.string.tutorial_what_new),
	FIRST_START("tutorial_first_start", R.string.tutorial_first_start),
	ULEY("tutorial_uley", R.string.tutorial_uley),
	VISIBILITY("tutorial_visibility", R.string.tutorial_visibility),
	CONTACTS("tutorial_contacts", R.string.tutorial_contacts),
	NEAR_TAB("tutorial_near_tab", R.string.tutorial_near_tab),
	HISTORY_TAB("tutorial_history_tab", R.string.tutorial_history_tab),
	WINK_OUTGOING("tutorial_wink_outgoing", R.string.tutorial_wink_outgoing),
	WINK_INCOME("tutorial_wink_income", R.string.tutorial_wink_income);

	private String prefName;
	private int resourceID;

	public int getReqCode()
	{
		return resourceID;
	}

	public String getPrefName()
	{
		return prefName;
	}

	private Tutorials(String prefName, int resourceID)
	{
		this.prefName = prefName;
		this.resourceID = resourceID;
	}

	public TutorialDialogFragment show(Fragment fragment)
	{
		SharedPreferences prefs = fragment.getActivity().getSharedPreferences("tutorials", Context.MODE_PRIVATE);
		if(!prefs.getBoolean(prefName, false) && prefs.getBoolean(ENABLED.prefName, true))
		{
			TutorialDialogFragment tutorialDialog = new TutorialDialogFragment();

			FragmentTransaction ft = fragment.getFragmentManager().beginTransaction();
			Fragment prev = fragment.getFragmentManager().findFragmentByTag("tutorial_dialog");
			if(prev != null)
			{
				ft.remove(prev);
			}
			ft.addToBackStack(null);
			ft.commit();

			tutorialDialog.setMessage(fragment.getString(resourceID));
			tutorialDialog.setTargetFragment(fragment, getReqCode());
			tutorialDialog.show(fragment.getFragmentManager(), "tutorial_dialog");

			prefs.edit().putBoolean(prefName, true).apply();

			return tutorialDialog;
		}

		return null;
	}

	public TutorialDialogFragment show(FragmentActivity activity)
	{
		SharedPreferences prefs = activity.getSharedPreferences("tutorials", Context.MODE_PRIVATE);
		if(!prefs.getBoolean(prefName, false) && prefs.getBoolean(ENABLED.prefName, true))
		{
			TutorialDialogFragment tutorialDialog = new TutorialDialogFragment();

			FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("tutorial_dialog");
			if(prev != null)
			{
				ft.remove(prev);
			}
			ft.addToBackStack(null);
			ft.commit();

			tutorialDialog.setMessage(activity.getString(resourceID));
			tutorialDialog.show(activity.getSupportFragmentManager(), "tutorial_dialog");

			prefs.edit().putBoolean(prefName, true).apply();

			return tutorialDialog;
		}

		return null;
	}
}

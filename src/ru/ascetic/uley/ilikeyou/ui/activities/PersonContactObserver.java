package ru.ascetic.uley.ilikeyou.ui.activities;

import ru.ascetic.uley.ilikeyou.objects.SocialUser;

public interface PersonContactObserver
{
	public void onPersonReceived(SocialUser person, String event, int[] messageIDs, boolean is_like_received, boolean is_like_delivered);
}

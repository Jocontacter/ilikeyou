package ru.ascetic.uley.ilikeyou.ui.activities;

import java.sql.SQLException;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import ru.ascetic.uley.ilikeyou.ui.adapters.LazyAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class HistoryListFragment extends ListFragment implements PersonContactObserver
{
	public static final int TAB_ORDER = 2;

	private List<SocialUser> persons = null;
	private LazyAdapter adapter;

	public void onPersonReceived(SocialUser person, String event, int[] messageIDs, boolean is_like_received, boolean is_like_delivered)
	{
		if(event.equals("clear"))
		{
			fillListView();
			return;
		}

		if(event.equals("update"))
		{
			updatePerson(person);
		}
		else if(event.equals("add"))
		{
			persons.add(0, person);
		}

		adapter.notifyDataSetChanged();
	}

	/** заменяет элемент в списке новым пользователем с идентичным gid'ом **/
	private void updatePerson(SocialUser person)
	{
		for(int i = 0; i < persons.size(); i++)
		{
			SocialUser user = persons.get(i);

			if(user.gid.equals(person.gid))
			{
				persons.remove(i);
				persons.add(0, person);
				return;
			}
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.page_personlist, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		fillListView();
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);

		String gid = ((SocialUser) l.getItemAtPosition(position)).gid;

		Intent intent = new Intent(getActivity(), UserDetailsActivity.class);
		intent.putExtra("gid", gid);
		startActivity(intent);
	}

	private void fillListView()
	{
		try
		{
			persons = HelperFactory.getHelper().getSocialUserDAO().getAllUsers();
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			return;
		}

		adapter = new LazyAdapter(this.getActivity(), persons);
		getListView().setAdapter(adapter);
	}
}

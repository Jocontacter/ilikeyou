﻿package ru.ascetic.uley.ilikeyou.ui.activities;

import java.util.ArrayList;
import java.util.Arrays;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import ru.ascetic.uley.ilikeyou.objects.social.FbHelper;
import ru.ascetic.uley.ilikeyou.objects.social.VkAccount;
import ru.ascetic.uley.ilikeyou.objects.social.VkAccount.VKAutorizationListener;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.InternetDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.ProfileImportDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.ProfileImportDialogFragment.ImportDialogResultListener;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.vk.sdk.VKUIHelper;

public class RegisterActivity extends FragmentActivity implements ImportDialogResultListener, VKAutorizationListener
{
	private static final String TAG = "ily";

	private VkAccount vkAccount;
	private GraphUser fbUser;

	private UiLifecycleHelper fbUiHelper;

	private Profile userProfile;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_social_register);

		userProfile = Profile.getInstance(RegisterActivity.this);

		vkAccount = new VkAccount(this, getString(R.string.vk_app_id));

		fbUiHelper = new UiLifecycleHelper(this, callback);
		fbUiHelper.onCreate(savedInstanceState);
		VKUIHelper.onCreate(this);

		ImageButton vkButton = (ImageButton) findViewById(R.id.vk_register_button);
		LoginButton fbButton = (LoginButton) findViewById(R.id.fb_register_button);
		fbButton.setReadPermissions(Arrays.asList("user_birthday", "user_photos", "user_about_me"));

		fbButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback()
		{
			@Override
			public void onUserInfoFetched(GraphUser user)
			{
				if(user != null)
				{
					fbUser = user;
					Log.i("fb_user", user.getInnerJSONObject().toString());

					ProfileImportDialogFragment importDialog = new ProfileImportDialogFragment(CommunicationType.FACEBOOK);
					importDialog.show(getSupportFragmentManager(), "import decision");

					Session.getActiveSession().closeAndClearTokenInformation();
				}
			}
		});

		vkButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// due to: com.perm.kate.api.KException:
				// User authorization failed: access_token was given to another ip address.
				vkAccount.logOut();

				if(internet())
				{
					vkAccount.autorize(true, false, RegisterActivity.this);
				}
				else
				{
					InternetDialogFragment internetDialog = new InternetDialogFragment();
					internetDialog.show(RegisterActivity.this.getSupportFragmentManager(), "internet notice");
				}
			}
		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		fbUiHelper.onResume();
		VKUIHelper.onResume(this);

		// Call the 'activateApp' method to log an app event for use in analytics and advertising reporting. Do so in
		// the onResume methods of the primary Activities that an app may be launched into.
		AppEventsLogger.activateApp(this);

		Tutorials.WHAT_NEW.show(this);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		fbUiHelper.onPause();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		fbUiHelper.onDestroy();
		VKUIHelper.onDestroy(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		fbUiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		fbUiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
		VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
	}

	private Session.StatusCallback callback = new Session.StatusCallback()
	{
		@Override
		public void call(Session session, SessionState state, Exception exception)
		{
			if(session != null && session.isOpened())
			{
				if(!state.equals(SessionState.OPENED_TOKEN_UPDATED))
				{
					// makeMeRequest(session);
				}
			}

			if(state.isOpened())
			{
				Log.i(TAG, "Logged in...");
			}
			else if(state.isClosed())
			{
				Log.i(TAG, "Logged out...");

				if(state.equals(SessionState.CLOSED_LOGIN_FAILED))
				{
					if(exception != null)
					{
						if(!internet())
						{
							InternetDialogFragment internetDialog = new InternetDialogFragment();
							internetDialog.show(RegisterActivity.this.getSupportFragmentManager(), "internet notice");
						}
					}
				}
			}
		}
	};

	@SuppressWarnings("unused")
	private void makeMeRequest(final Session session)
	{
		Request request = Request.newMeRequest(session, new Request.GraphUserCallback()
		{
			@Override
			public void onCompleted(GraphUser user, Response response)
			{
				if(session == Session.getActiveSession())
				{
					Log.i("fb_user from makeMeRequest", user.getInnerJSONObject().toString());
				}
				if(response.getError() != null)
				{

				}
			}
		});
		request.executeAsync();
	}

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback()
	{
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data)
		{
			Log.d("iLikeYou", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data)
		{
			Log.d("iLikeYou", "Success!");
		}
	};

	public void onIncognitoClick(View v)
	{
		userProfile.nick = "Incognito";
		userProfile.saveGeneral();
		startActivity(new Intent(RegisterActivity.this, MainActivity.class));
		finish();
	}

	private boolean internet()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		return isConnected;
	}

	@Override
	public void onImportDialogResult(final CommunicationType type, final ArrayList<String> import_fields)
	{
		if(import_fields.size() > 0)
		{
			// Общение с сервером в отдельном потоке чтобы не блокировать UI поток
			new Thread()
			{
				@Override
				public void run()
				{
					boolean success = false;
					switch(type)
					{
						case VKONTAKTE:
							success = vkAccount.linkProfile(userProfile, import_fields);
							break;
						case FACEBOOK:
							success = new FbHelper(RegisterActivity.this).setUserLink(fbUser, userProfile).fillFbUserInfo(import_fields);
							break;
						default:
							throw new IllegalArgumentException("Импорт для данного типа соцсетей не поддерживается");
					}

					if(success)
					{
						// Завершаем в UI потоке
						runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								startActivity(new Intent(RegisterActivity.this, MainActivity.class));
								finish();
							}
						});
					}
					else
					{
						Toast.makeText(RegisterActivity.this, "Ошибка при импорте из " + type.name(), Toast.LENGTH_LONG).show();
					}
				}
			}.start();
		}
	}

	@Override
	public void onAutorize(boolean success)
	{
		if(success)
		{
			ProfileImportDialogFragment importDialog = new ProfileImportDialogFragment(CommunicationType.VKONTAKTE);
			importDialog.show(getSupportFragmentManager(), "import decision");
		}
		else
		{
			Toast.makeText(this, "Ошибка при авторизации VKontakte", Toast.LENGTH_LONG).show();
		}
	}
}
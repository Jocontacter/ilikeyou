﻿package ru.ascetic.uley.ilikeyou.ui.activities;

import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.Profile.Contact;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProfileFragment extends Fragment implements PersonContactObserver
{
	public static final int TAB_ORDER = 0;
	private final static int REQ_CODE_PROFILE_EDIT = 0110;

	private Profile userProfile;
	private EditText statusTextView;
	private TextView nickname;
	private TextView likesNum;
	private TextView birthdateTextView;
	private ImageView avatarView;
	private ImageButton saveStatusButton;
	private LinearLayout contactList;

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		userProfile = Profile.getInstance(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.page_profile, container, false);

		nickname = (TextView) view.findViewById(R.id.nickname);
		statusTextView = (EditText) view.findViewById(R.id.status);
		birthdateTextView = (TextView) view.findViewById(R.id.birthdate);
		likesNum = (TextView) view.findViewById(R.id.likesNum);

		avatarView = (ImageView) view.findViewById(R.id.avatarImageView);
		saveStatusButton = (ImageButton) view.findViewById(R.id.saveStatusButton);
		saveStatusButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String status = statusTextView.getText().toString();
				userProfile.status = status;
				userProfile.saveGeneral();
				v.setVisibility(View.INVISIBLE);
				saveStatusButton.clearFocus();
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(statusTextView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		});

		contactList = (LinearLayout) view.findViewById(R.id.contacts);

		TextView profile_edit = (TextView) view.findViewById(R.id.profile_edit_tv);
		profile_edit.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				startActivityForResult(new Intent(getActivity(), UserSettingsActivity.class), REQ_CODE_PROFILE_EDIT);
			}
		});

		statusTextView.addTextChangedListener(new TextWatcher()
		{

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				String status = s.toString();
				boolean statusChanged = !status.equals(userProfile.status);
				if(statusChanged && saveStatusButton.getVisibility() != View.VISIBLE)
				{
					saveStatusButton.setVisibility(View.VISIBLE);
				}
				else if(!statusChanged && saveStatusButton.getVisibility() != View.INVISIBLE)
				{
					saveStatusButton.setVisibility(View.INVISIBLE);
				}
			}
		});

		return view;
	}

	private void setProfileContent()
	{
		nickname.setText(userProfile.nick);
		statusTextView.setText(userProfile.status);

		likesNum.setText(String.valueOf(userProfile.getIncomingLikes().size()));

		if(userProfile.imageBytes != null)
		{
			avatarView.setImageBitmap(userProfile.getImage());
		}

		birthdateTextView.setText(Util.getAgeString(this.getActivity(), userProfile.getAge()));

		contactList.removeAllViews();
		inflateContacts();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		setProfileContent();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		Tutorials.FIRST_START.show(this);
	}

	@Override
	public void onDetach()
	{
		userProfile = null;
		super.onDetach();
	}

	private void inflateContacts()
	{
		LayoutInflater inflater = getActivity().getLayoutInflater();

		List<Contact> contacts = userProfile.getProfileContacts();

		for(int i = 0; i < contacts.size(); i++)
		{
			Contact contact = contacts.get(i);
			addCommunicationButton(contact, inflater);
		}
	}

	private void addCommunicationButton(Contact contact, LayoutInflater inflater)
	{
		if(inflater == null)
			inflater = LayoutInflater.from(getActivity());

		View vi = inflater.inflate(R.layout.item_contacts_simple, contactList, false);
		TextView contactValue = (TextView) vi.findViewById(R.id.value);
		TextView visibilityTextView = (TextView) vi.findViewById(R.id.txt_visibility);
		ImageView contactLogo = (ImageView) vi.findViewById(R.id.logoImage);

		switch(contact.type)
		{
			case FACEBOOK:
			case VKONTAKTE:
			case TWITTER:
				int slashPosition = contact.value.lastIndexOf("/");
				String nick = contact.value.substring(slashPosition + 1);
				contactValue.setText(nick);
				break;
			default:
				contactValue.setText(contact.value);
				break;
		}

		contactLogo.setImageBitmap(BitmapFactory.decodeResource(getResources(), contact.type.getDrawableResource()));

		vi.setTag(contact.type);

		// TODO: вынести в локализацию!
		int visibility = 0;
		int color = 0;

		switch(contact.visibility)
		{
			case Invisible:
				visibility = R.string.invisible;
				color = getResources().getColor(R.color.visibility_text_invisible);
				break;

			case Semivisible:
				visibility = R.string.semivisible;
				color = getResources().getColor(R.color.visibility_text_semivisible);
				break;

			case Visible:
				visibility = R.string.visible;
				color = getResources().getColor(R.color.visibility_text_visible);
				break;

			default:
				break;
		}
		visibilityTextView.setText(getString(visibility));
		visibilityTextView.setTextColor(color);

		contactList.addView(vi, contactList.getChildCount() - 1);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);

		switch(requestCode)
		{
			case REQ_CODE_PROFILE_EDIT:
				setProfileContent();
				break;
		}
	}

	public static int getTabOrder()
	{
		return TAB_ORDER;
	}

	public static String getTabName()
	{
		return "android:switcher:" + R.id.pager + ":" + TAB_ORDER;
	}

	public interface OnDialogTextChangedListener
	{
		public void onDialogTextChanged(Object tag, String text);
	}

	@Override
	public void onPersonReceived(SocialUser person, String event, int[] messageIDs, boolean is_like_received, boolean is_like_delivered)
	{
		if(event.equals("clear"))
		{
			likesNum.setText(String.valueOf(0));
		}
		else if(is_like_received)
		{
			likesNum.setText(String.valueOf(userProfile.getIncomingLikes().size()));
		}
	}
}

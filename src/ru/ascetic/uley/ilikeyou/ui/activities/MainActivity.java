﻿package ru.ascetic.uley.ilikeyou.ui.activities;

import java.sql.SQLException;
import java.util.Locale;

import ru.ascetic.uley.ServiceSwitcher;
import ru.ascetic.uley.ServiceSwitcher.IBluetoothRequestStarter;
import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import ru.ascetic.uley.ilikeyou.sync.UleyDataProvider;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.TutorialDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.YesNoDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.YesNoDialogFragment.OnYesSelectedListener;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.vk.sdk.VKUIHelper;

public class MainActivity extends ActionBarActivity implements OnPageChangeListener
{
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will keep every loaded fragment in memory.
	 * If this becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;

	private Menu menu;

	private SharedPreferences prefs = null;
	private ServiceSwitcher serviceSwitcher;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if(Profile.getInstance(this).nick.equals(""))
		{
			Intent startIntent;
			startIntent = new Intent(getApplicationContext(), RegisterActivity.class);
			startActivity(startIntent);
			finish();
			return;
		}

		setContentView(R.layout.activity_main);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);

		mViewPager.setOnPageChangeListener(this);

		mViewPager.setAdapter(mSectionsPagerAdapter);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		prefs.registerOnSharedPreferenceChangeListener(prefsListener);

		serviceSwitcher = ServiceSwitcher.getInstance();
		serviceSwitcher.setContext(this);
		serviceSwitcher.registerPreferenceCallback();
		serviceSwitcher.setBluetoothRequestStarter(new IBluetoothRequestStarter()
		{
			@Override
			public void startBluetoothRequest()
			{
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, ServiceSwitcher.REQUEST_ENABLE_BT);
			}
		});

		if(!prefs.contains("service_enabled"))
		{
			prefs.edit().putBoolean("service_enabled", true).apply();
		}
		else
		{
			boolean enabled = prefs.getBoolean("service_enabled", true);
			serviceSwitcher.turnState(enabled);
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		LocalBroadcastManager.getInstance(this).registerReceiver(personReceiver, new IntentFilter(UleyDataProvider.PERSON_RECEIVED));
	}

	@Override
	public void onStop()
	{
		LocalBroadcastManager.getInstance(this).unregisterReceiver(personReceiver);
		super.onStop();
	}

	private BroadcastReceiver personReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String event = intent.getStringExtra("event");
			String gid = intent.getStringExtra("gid");
			int[] messageIDs = null;
			boolean is_like_received = false;
			boolean is_like_delivered = false;

			SocialUser person = null;
			try
			{
				person = HelperFactory.getHelper().getSocialUserDAO().getUserByGid(gid);
				if(person == null)
					return;
			}
			catch(SQLException e)
			{
				Log.e("ily", "Exception", e);
				return;
			}

			if(event.equals("update"))
			{
				messageIDs = intent.getIntArrayExtra("message_ids");
				is_like_received = intent.getBooleanExtra("is_like_received", false);
				is_like_delivered = intent.getBooleanExtra("is_like_received_on_remote", false);
			}

			FragmentPagerAdapter adapter = (FragmentPagerAdapter) mViewPager.getAdapter();

			((PersonContactObserver) adapter.getItem(NearListFragment.TAB_ORDER)).onPersonReceived(person, event, messageIDs, is_like_received,
					is_like_delivered);
			((PersonContactObserver) adapter.getItem(HistoryListFragment.TAB_ORDER)).onPersonReceived(person, event, messageIDs, is_like_received,
					is_like_delivered);
			((PersonContactObserver) adapter.getItem(ProfileFragment.TAB_ORDER)).onPersonReceived(person, event, messageIDs, is_like_received,
					is_like_delivered);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// DO NOT REMOVE THIS CALLING OF SUPERMETHOD!!! NEED FOR CORRECT DELEGATE TO FRAGMENTS!!! WAS SPENT A LOT OF
		// TIME!
		// SEE
		// http://www.androiddesignpatterns.com/2013/08/fragment-transaction-commit-state-loss.html
		// SEE
		// http://stackoverflow.com/questions/17085729/startactivityforresult-from-a-fragment-and-finishing-child-activity-doesnt-c
		super.onActivityResult(requestCode, resultCode, data);
		VKUIHelper.onActivityResult(this, requestCode, resultCode, data);

		if(requestCode == ServiceSwitcher.REQUEST_ENABLE_BT)
		{
			if(resultCode != RESULT_OK)
			{
				Toast.makeText(this, getString(R.string.service_will_be_started), Toast.LENGTH_SHORT).show();
			}
		}
		else if(requestCode == Tutorials.ULEY.getReqCode())
		{
			Intent intent = new Intent(this, ru.ascetic.uley.ui.MainActivity.class);
			startActivity(intent);
		}
	}

	private OnSharedPreferenceChangeListener prefsListener = new OnSharedPreferenceChangeListener()
	{

		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
		{
			if(key.equals("service_enabled"))
			{
				if(menu != null)
				{
					boolean enabled = sharedPreferences.getBoolean("service_enabled", true);
					MenuItem item = menu.findItem(R.id.action_settings_service_state);

					toggleMenuItem(item, enabled);
				}
			}
		}
	};

	private void toggleMenuItem(MenuItem item, boolean enabled)
	{
		item.setChecked(enabled);

		item.setTitle(enabled ? R.string.action_service_enabled : R.string.action_service_disabled);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mainmenu, menu);

		this.menu = menu;

		boolean enabled = prefs.getBoolean("service_enabled", true);
		MenuItem item = menu.findItem(R.id.action_settings_service_state);

		toggleMenuItem(item, enabled);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.action_clear:

				YesNoDialogFragment yesnodialog = new YesNoDialogFragment(getString(R.string.yesno_attention_title),
						getString(R.string.yesno_clear_users_message));
				yesnodialog.setListener(new OnYesSelectedListener()
				{

					@Override
					public void onYesSelected()
					{
						try
						{
							HelperFactory.getHelper().getSocialUserDAO().deleteBuilder().delete();
							HelperFactory.getHelper().getMessageDAO().deleteBuilder().delete();
						}
						catch(SQLException e)
						{
							Log.e("MainActivity", "Clear data fail", e);

						}

						FragmentPagerAdapter adapter = (FragmentPagerAdapter) mViewPager.getAdapter();

						((PersonContactObserver) adapter.getItem(NearListFragment.TAB_ORDER)).onPersonReceived(null, "clear", null, false, false);
						((PersonContactObserver) adapter.getItem(HistoryListFragment.TAB_ORDER)).onPersonReceived(null, "clear", null, false, false);
						((PersonContactObserver) adapter.getItem(ProfileFragment.TAB_ORDER)).onPersonReceived(null, "clear", null, false, false);
					}

					@Override
					public void onNoSelected()
					{
					}
				});

				yesnodialog.show(getSupportFragmentManager(), "clear_users_dialog");
				break;
			case R.id.action_share:
				startActivity(new Intent(this, QrShareActivity.class));
				break;
			case R.id.action_uley:

				TutorialDialogFragment dialog = Tutorials.ULEY.show(this);
				if(dialog != null)
				{
					dialog.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							Intent intent = new Intent(MainActivity.this, ru.ascetic.uley.ui.MainActivity.class);
							startActivity(intent);
						}
					});
				}
				else
				{
					Intent intent = new Intent(this, ru.ascetic.uley.ui.MainActivity.class);
					startActivity(intent);
				}

				break;

			case R.id.action_settings_service_state:
				Tutorials.VISIBILITY.show(this);

				boolean enabled = prefs.getBoolean("service_enabled", true);
				SharedPreferences.Editor editor = prefs.edit();

				enabled = !enabled;

				editor.putBoolean("service_enabled", enabled);
				toggleMenuItem(item, enabled);

				editor.commit();
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter
	{
		Fragment[] fragments = new Fragment[]
			{ new ProfileFragment(), new NearListFragment(), new HistoryListFragment() };

		public SectionsPagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			return fragments[position];
		}

		@Override
		public int getCount()
		{
			return fragments.length;
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			Locale l = Locale.getDefault();
			switch(position)
			{
				case ProfileFragment.TAB_ORDER:
					return getString(R.string.title_profile).toUpperCase(l);
				case NearListFragment.TAB_ORDER:
					return getString(R.string.title_near).toUpperCase(l);
				case HistoryListFragment.TAB_ORDER:
					return getString(R.string.title_history).toUpperCase(l);
			}
			return null;
		}
	}

	@Override
	public void onPageScrollStateChanged(int pos)
	{
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2)
	{
	}

	@Override
	public void onPageSelected(int pos)
	{
		switch(pos)
		{
			case NearListFragment.TAB_ORDER:
				Tutorials.NEAR_TAB.show(this);
				break;
			case HistoryListFragment.TAB_ORDER:
				Tutorials.HISTORY_TAB.show(this);
				break;
		}
	}
}

package ru.ascetic.uley.ilikeyou.ui.activities.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class YesNoDialogFragment extends DialogFragment
{
	public static final int RESULT_CODE_NO_SELECTED = 0;
	public static final int RESULT_CODE_YES_SELECTED = 1;

	private String title;
	private String message;

	private OnYesSelectedListener listener = new OnYesSelectedListener()
	{
		@Override
		public void onYesSelected()
		{
		}

		@Override
		public void onNoSelected()
		{
		}
	};

	/**
	 * @param listener
	 *            the listener to set
	 **/
	public void setListener(OnYesSelectedListener listener)
	{
		this.listener = listener;
	}

	public interface OnYesSelectedListener
	{
		public void onYesSelected();

		public void onNoSelected();
	}

	public YesNoDialogFragment()
	{
		// Empty constructor required for DialogFragment
	}

	public YesNoDialogFragment(String title, String message)
	{
		this.title = title;
		this.message = message;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();

		if(args != null)
		{
			this.title = args.getString("title");
			this.message = args.getString("message");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title).setMessage(message).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				listener.onNoSelected();
				dialog.dismiss();
			}
		}).setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				if(getTargetFragment() != null)
					getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_CODE_YES_SELECTED, null);

				listener.onYesSelected();
				dialog.dismiss();
			}
		});
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
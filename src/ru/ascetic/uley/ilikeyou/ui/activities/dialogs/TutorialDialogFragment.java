package ru.ascetic.uley.ilikeyou.ui.activities.dialogs;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.ui.activities.Tutorials;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TutorialDialogFragment extends DialogFragment
{
	private String text;

	DialogInterface.OnClickListener onOkClickListener;

	public TutorialDialogFragment()
	{
		// Empty constructor required for DialogFragment
	}

	public void setMessage(String text)
	{
		this.text = text;
	}

	public void setOnClickListener(DialogInterface.OnClickListener onOkClickListener)
	{
		this.onOkClickListener = onOkClickListener;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(text).setMultiChoiceItems(new String[]
			{ getString(R.string.tutorial_enabled) }, new boolean[]
			{ true }, new OnMultiChoiceClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked)
			{
				if(!isChecked)
				{
					getActivity().getPreferences(Context.MODE_PRIVATE).edit().putBoolean(Tutorials.ENABLED.getPrefName(), false).commit();
				}
			}
		}).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.dismiss();

				if(onOkClickListener != null)
				{
					onOkClickListener.onClick(dialog, id);
				}
			}
		});

		// Create the AlertDialog object and return it
		return builder.create();
	}
}
﻿package ru.ascetic.uley.ilikeyou.ui.activities.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.SimpleAdapter;

public class AddCommunicationDialogFragment extends DialogFragment
{
	private SimpleAdapter adapter;

	public AddCommunicationDialogFragment()
	{
		// Empty constructor required for DialogFragment
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		String[] comm_keys = getArguments().getStringArray("to_show");

		// Each row in the list stores country name, currency and flag
		List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

		for(int i = 0; i < comm_keys.length; i++)
		{
			CommunicationType type = CommunicationType.getByKey(comm_keys[i]);

			HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("name", getActivity().getString(type.getStringResource()));
			hm.put("image", Integer.toString(type.getDrawableResource()));
			hm.put("key", comm_keys[i]);
			aList.add(hm);
		}

		// Instantiating an adapter to store each items
		// R.layout.listview_layout defines the layout of each item
		adapter = new SimpleAdapter(getActivity(), aList, R.layout.item_communication_for_addition, new String[]
			{ "name", "image" }, new int[]
			{ R.id.name, R.id.logoImage });

		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.dialog_select_communication).setAdapter(adapter, new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				@SuppressWarnings("unchecked")
				HashMap<String, String> item = (HashMap<String, String>) adapter.getItem(which);
				Intent result = new Intent();
				result.putExtra("key", item.get("key"));
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);
			}
		}).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.dismiss();
			}
		});

		return builder.create();
	}
}
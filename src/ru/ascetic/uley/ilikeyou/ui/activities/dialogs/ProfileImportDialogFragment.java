﻿package ru.ascetic.uley.ilikeyou.ui.activities.dialogs;

import java.util.ArrayList;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ProfileImportDialogFragment extends DialogFragment
{
	private boolean[] to_import;

	private String comType;

	public static interface ImportDialogResultListener
	{
		public void onImportDialogResult(CommunicationType type, ArrayList<String> import_fields);
	}

	public ProfileImportDialogFragment(CommunicationType commType)
	{
		comType = commType.getKey();
	}

	public ProfileImportDialogFragment()
	{
		// Empty constructor required for DialogFragment
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle b = getArguments();
		if(b != null)
		{
			comType = b.getString("communicationType");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		to_import = new boolean[]
			{ true, true, true };
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.form_import).setMultiChoiceItems(R.array.import_fields, to_import, new OnMultiChoiceClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked)
			{
				to_import[which] = isChecked;
			}
		}).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				dialog.dismiss();

				String[] profile_fields = getResources().getStringArray(R.array.import_fields);
				ArrayList<String> import_fields = new ArrayList<String>();

				for(int i = 0; i < to_import.length; i++)
				{
					if(to_import[i])
					{
						import_fields.add(profile_fields[i]);
					}
				}

				Intent result = new Intent();
				result.putExtra("fields_to_import", import_fields);
				result.putExtra("communicationType", comType);
				if(getTargetFragment() != null)
					getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);
				else
					((ImportDialogResultListener) getActivity()).onImportDialogResult(CommunicationType.getByKey(comType), import_fields);
			}
		}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		// Create the AlertDialog object and return it
		return builder.create();
	}
}
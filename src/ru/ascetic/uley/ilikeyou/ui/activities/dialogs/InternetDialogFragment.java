﻿package ru.ascetic.uley.ilikeyou.ui.activities.dialogs;

import ru.ascetic.uley.ilikeyou.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;

public class InternetDialogFragment extends DialogFragment
{
	public InternetDialogFragment()
	{
		// Empty constructor required for DialogFragment
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.internet_settings)
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						dialog.dismiss();
					}
				})
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						Intent internetSettings = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
						startActivity(internetSettings);
						dialog.dismiss();
					}
				});
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
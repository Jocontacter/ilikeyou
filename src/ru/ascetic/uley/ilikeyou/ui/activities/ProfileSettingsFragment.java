package ru.ascetic.uley.ilikeyou.ui.activities;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.Profile.Contact;
import ru.ascetic.uley.ilikeyou.objects.Profile.ContactVisibility;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import ru.ascetic.uley.ilikeyou.objects.social.FbHelper;
import ru.ascetic.uley.ilikeyou.objects.social.VkAccount;
import ru.ascetic.uley.ilikeyou.objects.social.VkAccount.VKAutorizationListener;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.AddCommunicationDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.DatePickerDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.InternetDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.activities.dialogs.ProfileImportDialogFragment;
import ru.ascetic.uley.ilikeyou.ui.views.SegmentedRadioGroup;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.vk.sdk.VKUIHelper;

public class ProfileSettingsFragment extends Fragment implements OnCheckedChangeListener, VKAutorizationListener
{
	public static final int TAB_ORDER = 0;

	private static final int ACTION_ID_EDIT = 1;
	private static final int ACTION_ID_REMOVE = 2;

	private final static int REQ_CODE_PICK_IMAGE = 0101;
	private final static int REQ_CODE_PICK_DATE = 0102;
	private static final int REQ_CODE_ADD_COMMUNICATION = 0103;
	private static final int REQ_CODE_IMPORT_PROFILE = 0104;

	private Profile userProfile;

	private VkAccount vkAccount;
	private UiLifecycleHelper fbUiHelper;
	private LoginButton fbButton;
	private GraphUser fbUser;

	private EditText nickname;
	private TextView birthdateTextView;
	private ImageView avatarView;
	private SegmentedRadioGroup sexRadioGroup;
	private LinearLayout contactList;
	private View addButton;
	private QuickAction mQuickAction;
	private View selectedCommunicationView = null;

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		userProfile = Profile.getInstance(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		fbUiHelper = new UiLifecycleHelper(getActivity(), new Session.StatusCallback()
		{
			@Override
			public void call(Session session, SessionState state, Exception exception)
			{
				if(state.isOpened())
				{
					Log.i("ProfileFragment-facebook", "Logged in...");
				}
				else if(state.isClosed())
				{
					Log.i("ProfileFragment-facebook", "Logged out...");
				}
			}
		});

		VKUIHelper.onCreate(getActivity());

		fbUiHelper.onCreate(savedInstanceState);

		vkAccount = new VkAccount(this.getActivity(), getString(R.string.vk_app_id));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.page_user_settings, container, false);

		fbButton = (LoginButton) view.findViewById(R.id.fb_register_button);
		fbButton.setFragment(this);
		fbButton.setReadPermissions(Arrays.asList("user_birthday", "user_photos", "user_about_me"));

		fbButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback()
		{
			@Override
			public void onUserInfoFetched(GraphUser user)
			{
				if(user != null)
				{
					Log.i("fb_user", user.getInnerJSONObject().toString());

					Session.getActiveSession().closeAndClearTokenInformation();

					fbUser = user;

					ProfileImportDialogFragment importDialog = new ProfileImportDialogFragment();
					importDialog.setTargetFragment(ProfileSettingsFragment.this, REQ_CODE_IMPORT_PROFILE);
					Bundle args = new Bundle();
					args.putString("communicationType", CommunicationType.FACEBOOK.getKey());
					importDialog.setArguments(args);
					importDialog.show(getFragmentManager(), "import decision");
				}
			}
		});

		ActionItem editItem = new ActionItem(ACTION_ID_EDIT, getString(R.string.action_edit), getResources().getDrawable(
				android.R.drawable.ic_menu_edit));
		ActionItem deleteItem = new ActionItem(ACTION_ID_REMOVE, getString(R.string.action_remove), getResources().getDrawable(
				android.R.drawable.ic_menu_delete));
		mQuickAction = new QuickAction(this.getActivity());

		mQuickAction.addActionItem(editItem);
		mQuickAction.addActionItem(deleteItem);

		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener()
		{
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId)
			{
				// ActionItem actionItem = quickAction.getActionItem(pos);

				final CommunicationType selectedCommunicationType = (CommunicationType) selectedCommunicationView.getTag();

				switch(actionId)
				{
					case ACTION_ID_EDIT:
						String text = userProfile.getContacts().get(selectedCommunicationType.getKey());
						showEditDialog(selectedCommunicationType, text, new OnDialogTextChangedListener()
						{
							@Override
							public void onDialogTextChanged(Object tag, String text)
							{
								userProfile.putContact(new Contact(selectedCommunicationType, text, userProfile
										.getVisibility(selectedCommunicationType)));
								userProfile.saveContacts();
								((TextView) selectedCommunicationView.findViewById(R.id.value)).setText(text);
							}
						});
						break;
					case ACTION_ID_REMOVE:

						if(selectedCommunicationType == CommunicationType.VKONTAKTE)
						{
							vkAccount.logOut();
						}

						userProfile.removeContact(selectedCommunicationType);
						userProfile.saveContacts();
						removeCommunicationButton(selectedCommunicationType);
						break;
				}
			}
		});

		nickname = (EditText) view.findViewById(R.id.nickname);
		birthdateTextView = (TextView) view.findViewById(R.id.birthdate);

		avatarView = (ImageView) view.findViewById(R.id.avatarImageView);

		((ImageButton) view.findViewById(R.id.btnCalandar)).setOnClickListener(calendarClickListener);
		contactList = (LinearLayout) view.findViewById(R.id.contacts);

		sexRadioGroup = (SegmentedRadioGroup) view.findViewById(R.id.segment_sex);
		sexRadioGroup.setOnCheckedChangeListener(this);

		avatarView.setOnClickListener(imageClickListener);

		nickname.setOnFocusChangeListener(focusChangedListener);

		return view;
	}

	private void setProfileContent()
	{
		nickname.setText(userProfile.nick);

		if(userProfile.imageBytes != null)
		{
			avatarView.setImageBitmap(userProfile.getImage());
		}

		birthdateTextView.setText(getBirthdayAndAgeString(userProfile));

		setSexChecked(userProfile.sex);

		contactList.removeAllViews();
		inflateContacts();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		setProfileContent();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		Tutorials.FIRST_START.show(this);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		fbUiHelper.onResume();
		VKUIHelper.onResume(getActivity());
	}

	@Override
	public void onPause()
	{
		super.onPause();
		fbUiHelper.onPause();
	}

	@Override
	public void onStop()
	{
		super.onStop();
		fbUiHelper.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		fbUiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		fbUiHelper.onDestroy();
		VKUIHelper.onDestroy(getActivity());
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		if(savedInstanceState != null)
		{
			setProfileContent();
		}
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public void onDetach()
	{
		userProfile = null;
		super.onDetach();
	}

	private HashMap<SegmentedRadioGroup, TextView> stateTextHash = new HashMap<SegmentedRadioGroup, TextView>();

	private void inflateContacts()
	{
		LayoutInflater inflater = getActivity().getLayoutInflater();

		List<Contact> contacts = userProfile.getProfileContacts();
		CommunicationType[] types = CommunicationType.values();

		for(int i = 0; i < contacts.size(); i++)
		{
			Contact contact = contacts.get(i);
			addCommunicationButton(contact, inflater);
		}

		if(contacts.size() < types.length)
		{
			addAddButton(null);
		}
	}

	private void addCommunicationButton(Contact contact)
	{
		addCommunicationButton(contact, getActivity().getLayoutInflater());
	}

	private void addCommunicationButton(Contact contact, LayoutInflater inflater)
	{
		if(inflater == null)
			inflater = LayoutInflater.from(getActivity());

		View vi = inflater.inflate(R.layout.item_contacts, contactList, false);
		TextView contactValue = (TextView) vi.findViewById(R.id.value);
		TextView visibilityTextView = (TextView) vi.findViewById(R.id.txt_visibility);

		SegmentedRadioGroup visibilityGroup = (SegmentedRadioGroup) vi.findViewById(R.id.radio_visibility);
		ImageView contactLogo = (ImageView) vi.findViewById(R.id.logoImage);

		stateTextHash.put(visibilityGroup, visibilityTextView);
		visibilityGroup.setOnCheckedChangeListener(this);
		visibilityGroup.setTag(contact.type);

		switch(contact.type)
		{
			case FACEBOOK:
			case VKONTAKTE:
				int slashPosition = contact.value.lastIndexOf("/");
				String nick = contact.value.substring(slashPosition + 1);
				contactValue.setText(nick);
				break;
			default:
				contactValue.setText(contact.value);
				break;
		}

		contactLogo.setImageBitmap(BitmapFactory.decodeResource(getResources(), contact.type.getDrawableResource()));

		switch(contact.visibility)
		{
			case Invisible:
				visibilityGroup.check(R.id.contact_invisible);
				break;
			case Semivisible:
				visibilityGroup.check(R.id.contact_semivisible);
				break;
			case Visible:
				visibilityGroup.check(R.id.contact_visible);
				break;
			default:
				break;
		}

		vi.setTag(contact.type);

		vi.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				selectedCommunicationView = v;
				mQuickAction.show(v);
				mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);
			}
		});

		contactList.addView(vi, contactList.getChildCount() - 1);
	}

	private void addAddButton(LayoutInflater inflater)
	{
		if(inflater == null)
			inflater = getActivity().getLayoutInflater();

		addButton = inflater.inflate(R.layout.item_add_item, contactList, false);
		addButton.setOnClickListener(onAddCommunicationClickListener);
		contactList.addView(addButton);
	}

	/** Removes communication button corresponding to the type **/
	private void removeCommunicationButton(CommunicationType type)
	{
		int num = contactList.getChildCount() - 1;

		if(addButton == null)
		{
			// если последний элемент - не кнопка "добавить", тогда учитываем и его
			num++;
		}

		for(int i = 0; i < num; i++)
		{
			View v = contactList.getChildAt(i);
			CommunicationType vtype = (CommunicationType) v.getTag();

			// null may be caused by "add button"
			if(vtype != null && vtype.equals(type))
			{
				contactList.removeView(v);
			}
		}

		if(addButton == null)
		{
			addAddButton(null);
		}
	}

	OnClickListener onAddCommunicationClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			HashMap<String, String> contacts = userProfile.getContacts();
			CommunicationType[] types = CommunicationType.values();

			if(types.length - contacts.size() <= 0)
				return;

			String[] to_show = new String[types.length - contacts.size()];

			int ind = 0;
			for(int i = 0; i < types.length; i++)
			{
				if(!contacts.containsKey(types[i].getKey()))
				{
					to_show[ind] = types[i].getKey();
					ind++;
				}
			}

			AddCommunicationDialogFragment addCommunicationDialog = new AddCommunicationDialogFragment();
			Bundle args = new Bundle();
			args.putStringArray("to_show", to_show);
			addCommunicationDialog.setArguments(args);
			addCommunicationDialog.setTargetFragment(ProfileSettingsFragment.this, REQ_CODE_ADD_COMMUNICATION);
			addCommunicationDialog.show(ProfileSettingsFragment.this.getFragmentManager(), "communication choise");

			Tutorials.CONTACTS.show(ProfileSettingsFragment.this);
		}
	};

	OnFocusChangeListener focusChangedListener = new OnFocusChangeListener()
	{
		@Override
		public void onFocusChange(View v, boolean hasFocus)
		{
			boolean somethingChanged = false;

			if(!hasFocus)
			{
				if(nickname == v)
				{
					String nick = nickname.getText().toString();
					if(!nick.equals(userProfile.nick))
					{
						userProfile.nick = nick;
						somethingChanged = true;
					}
				}
			}

			if(somethingChanged)
				userProfile.saveGeneral();
		}
	};

	OnClickListener calendarClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			DialogFragment newFragment = new DatePickerDialogFragment();
			newFragment.setTargetFragment(ProfileSettingsFragment.this, REQ_CODE_PICK_DATE);
			newFragment.show(ProfileSettingsFragment.this.getFragmentManager(), "datePicker");
		}
	};

	OnClickListener imageClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if(isAdded())
			{
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);
			}
		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		fbUiHelper.onActivityResult(requestCode, resultCode, intent, null);

		if(resultCode == Activity.RESULT_OK)
			switch(requestCode)
			{
				case REQ_CODE_PICK_IMAGE:
					Uri selectedImage = intent.getData();
					setImage(selectedImage);
					break;
				case REQ_CODE_PICK_DATE:
					userProfile.birthday = intent.getLongExtra("dateAsMs", 0);
					birthdateTextView.setText(getBirthdayAndAgeString(userProfile));
					userProfile.saveGeneral();
					break;
				case REQ_CODE_ADD_COMMUNICATION:
					CommunicationType type = CommunicationType.getByKey(intent.getStringExtra("key"));

					switch(type)
					{
						case SKYPE:
						case EMAIL:
						case PHONE:
						case TWITTER:
							showEditDialog(type, "", contactEnteredListener);
							break;
						case VKONTAKTE:
							if(internet())
							{
								vkAccount.autorize(true, false, this);
							}
							else
							{
								InternetDialogFragment internetDialog = new InternetDialogFragment();
								internetDialog.show(this.getFragmentManager(), "internet notice");
							}
							break;
						case FACEBOOK:
							fbButton.performClick();
							break;
					}
					break;
				case REQ_CODE_IMPORT_PROFILE:
					final List<String> fields = intent.getStringArrayListExtra("fields_to_import");
					final CommunicationType comType = CommunicationType.getByKey(intent.getStringExtra("communicationType"));

					linkProfileToSocial(comType, fields);
					break;
			}
	}

	private void setImage(Uri selectedImage)
	{
		String[] filePathColumn =
			{ MediaStore.Images.Media.DATA };

		Cursor cursor = this.getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);

		if(cursor == null)
		{
			Toast.makeText(this.getActivity(), R.string.broken_image, Toast.LENGTH_LONG).show();
			return;
		}

		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();

		Bitmap selectedAvatar = Util.loadBitmap(filePath);

		if(selectedAvatar != null)
		{
			selectedAvatar = Util.getResizedBitmap(selectedAvatar, 300, 1199);
			avatarView.setImageBitmap(selectedAvatar);
			userProfile.setImage(selectedAvatar);
			userProfile.saveImage();
		}
		else
		{
			Toast.makeText(this.getActivity(), R.string.broken_image, Toast.LENGTH_LONG).show();
		}
	}

	private OnDialogTextChangedListener contactEnteredListener = new OnDialogTextChangedListener()
	{
		@Override
		public void onDialogTextChanged(Object tag, String text)
		{
			CommunicationType comType = (CommunicationType) tag;

			if(comType.equals(CommunicationType.TWITTER))
			{
				int usernamePosition = text.lastIndexOf("/") + 1;
				text = "twitter.com/" + text.substring(usernamePosition);
			}

			Contact contact = new Contact(comType, text, ContactVisibility.Invisible);
			userProfile.putContact(contact);
			userProfile.saveContacts();

			addCommunicationButton(contact);
			if(userProfile.getContacts().size() == CommunicationType.values().length)
			{
				contactList.removeView(addButton);
				addButton = null;
			}
		}
	};

	/**
	 * @param fields
	 * @param comType
	 **/
	private void linkProfileToSocial(final CommunicationType comType, final List<String> fields)
	{
		if(fields.size() > 0)
		{
			// Общение с сервером в отдельном потоке чтобы не блокировать UI поток
			new Thread()
			{
				@Override
				public void run()
				{
					boolean success = false;
					switch(comType)
					{
						case VKONTAKTE:
							success = vkAccount.linkProfile(userProfile, fields);
							break;
						case FACEBOOK:
							success = new FbHelper(getActivity()).setUserLink(fbUser, userProfile).fillFbUserInfo(fields);
							break;
						default:
							break;
					}

					if(success)
					{
						getActivity().runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								setProfileContent();
							}
						});
					}
				}
			}.start();
		}
	}

	public static int getTabOrder()
	{
		return TAB_ORDER;
	}

	public static String getTabName()
	{
		return "android:switcher:" + R.id.pager + ":" + TAB_ORDER;
	}

	private String getBirthdayAndAgeString(Profile profile)
	{
		Locale current = getResources().getConfiguration().locale;
		return profile.getBirthdayString(getString(R.string.template_birthday), current) + "("
				+ Util.getAgeString(this.getActivity(), profile.getAge()) + ")";
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId)
	{
		if(group == sexRadioGroup)
		{
			if(checkedId == R.id.button_male)
			{
				userProfile.sex = SocialUser.Sex.Male;
			}
			else if(checkedId == R.id.button_female)
			{
				userProfile.sex = SocialUser.Sex.Female;
			}
			else if(checkedId == R.id.button_unknown_sex)
			{
				userProfile.sex = SocialUser.Sex.Unknown;
			}
			userProfile.saveGeneral();
		}
		else
		{
			TextView visibilityTextView = stateTextHash.get(group);

			// TODO: вынести в локализацию!
			int visibility = 0;
			int color = 0;
			ContactVisibility visibilityValue = null;

			switch(checkedId)
			{
				case R.id.contact_invisible:
					visibilityValue = ContactVisibility.Invisible;
					visibility = R.string.invisible;
					color = getResources().getColor(R.color.visibility_text_invisible);
					;
					break;

				case R.id.contact_semivisible:
					visibilityValue = ContactVisibility.Semivisible;
					visibility = R.string.semivisible;
					color = getResources().getColor(R.color.visibility_text_semivisible);
					break;

				case R.id.contact_visible:
					visibilityValue = ContactVisibility.Visible;
					visibility = R.string.visible;
					color = getResources().getColor(R.color.visibility_text_visible);
					break;

				default:
					break;
			}
			visibilityTextView.setText(getString(visibility));
			visibilityTextView.setTextColor(color);

			userProfile.setVisibility((CommunicationType) group.getTag(), visibilityValue);
			userProfile.saveContacts();
		}
	}

	private void setSexChecked(SocialUser.Sex sex)
	{
		int id = R.id.button_unknown_sex;
		switch(sex)
		{
			case Male:
				id = R.id.button_male;
				break;
			case Female:
				id = R.id.button_female;
				break;
			default:
				break;
		}
		View item = getView().findViewById(id);
		if(item != null)
		{
			item.performClick();
		}
	}

	private void showEditDialog(final CommunicationType comType, String initialText, final OnDialogTextChangedListener listener)
	{
		// Set an EditText view to get user input
		final EditText input = new EditText(getActivity());
		input.setText(initialText);
		input.setInputType(comType.getInputType());

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle(R.string.communication_data_editing).setView(input)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(input.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

						listener.onDialogTextChanged(comType, input.getText().toString());
						dialog.dismiss();
					}
				}).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int whichButton)
					{
						InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(input.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

						dialog.dismiss();
					}
				});

		builder.show();
	}

	public interface OnDialogTextChangedListener
	{
		public void onDialogTextChanged(Object tag, String text);
	}

	private boolean internet()
	{
		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		return isConnected;
	}

	@Override
	public void onAutorize(boolean success)
	{
		if(success)
		{
			ProfileImportDialogFragment importDialog = new ProfileImportDialogFragment();
			importDialog.setTargetFragment(this, REQ_CODE_IMPORT_PROFILE);
			Bundle args = new Bundle();
			args.putString("communicationType", CommunicationType.VKONTAKTE.getKey());
			importDialog.setArguments(args);
			importDialog.show(getFragmentManager(), "import decision");
		}
		else
		{
			Toast.makeText(getActivity(), "Ошибка при авторизации VKontakte", Toast.LENGTH_LONG).show();
		}
	}
}

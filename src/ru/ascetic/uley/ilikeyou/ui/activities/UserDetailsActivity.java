﻿package ru.ascetic.uley.ilikeyou.ui.activities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.ascetic.uley.ilikeyou.LikeYouApplication;
import ru.ascetic.uley.ilikeyou.OffsideActivityHelper;
import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.Message;
import ru.ascetic.uley.ilikeyou.objects.Message.Direction;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import ru.ascetic.uley.ilikeyou.sync.UleyDataProvider;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserDetailsActivity extends ActionBarActivity
{
	private SocialUser person;

	private ImageView imageView;
	private ImageView ivWinkOutgoing;
	private ImageView ivWinkIncome;
	// private ImageView ivNearIndicator;
	private TextView nearTextView;
	private LinearLayout contactList;
	private LinearLayout messageList;
	private EditText newMessageEditText;
	private TextView nicknameTextView;
	private TextView statusTextView;

	private BroadcastReceiver messageReceiver;

	private Handler mHandler;

	private final Runnable mRunnable = new Runnable()
	{
		public void run()
		{
			long max_since = LikeYouApplication.SINCE_MAX;

			final long elapsed = System.currentTimeMillis() - person.lastConnection;

			if(elapsed > max_since)
			{
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						int color = Util.getNearColor(elapsed, getResources().getColor(R.color.color_since));
						nearTextView.setTextColor(color);
						nearTextView.setText(getString(R.string.near));
					}
				});
			}

			mHandler.postDelayed(this, LikeYouApplication.SINCE_MAX / 6);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_details);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);

		String personGid = getIntent().getStringExtra("gid");

		imageView = (ImageView) findViewById(R.id.avatarImage);
		nicknameTextView = (TextView) findViewById(R.id.nickname);
		statusTextView = (TextView) findViewById(R.id.textStatus);

		contactList = (LinearLayout) findViewById(R.id.contacts);
		messageList = (LinearLayout) findViewById(R.id.messages);
		newMessageEditText = (EditText) findViewById(R.id.new_message);
		ivWinkOutgoing = (ImageView) findViewById(R.id.ivWinkOutgoing);
		ivWinkIncome = (ImageView) findViewById(R.id.ivWinkIncome);
		// ivNearIndicator = (ImageView) findViewById(R.id.ivNearIndicator);

		nearTextView = (TextView) findViewById(R.id.since_duration);

		if(personGid != null)
			new AsyncPersonLoader().execute(personGid);

		mHandler = new Handler();

		messageReceiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context context, Intent intent)
			{
				String gid = intent.getStringExtra("gid");

				if(person == null || !gid.equals(person.gid))
					return;

				mHandler.post(mRunnable);

				boolean isChanged = intent.getBooleanExtra("is_changed", false);

				if(isChanged)
				{
					new AsyncPersonLoader().execute(gid);
					return;
				}

				int[] messageIDs = intent.getIntArrayExtra("message_ids");

				boolean is_like_received = intent.getBooleanExtra("is_like_received", false);
				// boolean is_like_received_on_remote = intent.getBooleanExtra("is_like_received_on_remote", false);

				addRecentMessages(messageIDs);

				if(is_like_received)
				{
					ivWinkIncome.setImageResource(R.drawable.wink_income_done);
				}
			}
		};
	}

	@Override
	public void onStart()
	{
		LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter(UleyDataProvider.MESSAGES_RECEIVED));
		super.onStart();
	}

	@Override
	public void onStop()
	{
		LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
		super.onStop();
	}

	public void onOutgoingClick(View v)
	{
		String help;
		if(person.likeTo)
		{
			help = "Вы подмигнули пользователю";
		}
		else
		{
			help = "Вы еще не подмигнули пользователю";
		}

		Toast.makeText(this, help, Toast.LENGTH_SHORT).show();
	}

	public void onIncomeClick(View v)
	{
		String help;
		if(person.likeTo)
		{
			help = "Пользователь подмигнул вам";
		}
		else
		{
			help = "Пользователь еще не подмигнул вам";
		}

		Toast.makeText(this, help, Toast.LENGTH_SHORT).show();
	}

	public void onSendMessageClick(View view)
	{
		String text = newMessageEditText.getText().toString();

		if(person == null || text.length() == 0)
			return;

		Message message = new Message();
		message.setDirection(Direction.From);
		message.setMessage(text);
		message.setRemoteGid(person.gid);

		try
		{
			message.setId(HelperFactory.getHelper().getMessageDAO().create(message));
			newMessageEditText.setText("");
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			return;
		}

		addMessageItem(message, LayoutInflater.from(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		if(Build.VERSION.SDK_INT >= 11)
		{
			selectMenu(menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		if(Build.VERSION.SDK_INT < 11)
		{
			selectMenu(menu);
		}
		return true;
	}

	private void selectMenu(Menu menu)
	{

		getMenuInflater().inflate(R.menu.user, menu);

		if(person != null)
		{
			if(person.likeTo)
			{
				menu.findItem(R.id.action_like).setEnabled(false);
			}
		}
		else
		{
			menu.findItem(R.id.action_like).setEnabled(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item)
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
				finish();
				break;
			case R.id.action_like:

				boolean likeNoticeIsShown = getPreferences(Context.MODE_PRIVATE).getBoolean("likeNoticeIsShown", false);

				if(!person.likeTo)
				{
					if(!likeNoticeIsShown)
					{
						showLikeNoticeAlert();
					}
				}
				break;

			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void showLikeNoticeAlert()
	{
		new AlertDialog.Builder(this).setMessage(R.string.dialog_like_notice)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{

						person.likeTo = true;

						supportInvalidateOptionsMenu();

						try
						{
							HelperFactory.getHelper().getSocialUserDAO().updateLightweightFields(person);
						}
						catch(SQLException e)
						{
							Log.e("ily", "Exception", e);
							return;
						}

						ivWinkOutgoing.setImageResource(R.drawable.wink_outgoing_done);
					}
				}).setNegativeButton(android.R.string.cancel, null).setMultiChoiceItems(new String[]
					{ getString(R.string.do_not_show_again) }, new boolean[]
					{ false }, new OnMultiChoiceClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked)
					{
						if(isChecked)
						{
							getPreferences(Context.MODE_PRIVATE).edit().putBoolean("likeNoticeIsShown", true).commit();
						}
						else
						{
							getPreferences(Context.MODE_PRIVATE).edit().putBoolean("likeNoticeIsShown", false).commit();
						}
					}
				}).create().show();
	}

	private void inflateContacts(HashMap<String, String> contacts)
	{
		LayoutInflater inflater = LayoutInflater.from(this);

		contactList.removeAllViews();

		CommunicationType[] types = CommunicationType.values();
		for(int i = 0; i < types.length; i++)
		{
			CommunicationType type = types[i];

			if(!contacts.containsKey(type.getKey()))
				continue;

			View vi = inflater.inflate(R.layout.item_simple_contacts, contactList, false);
			TextView contactValue = (TextView) vi.findViewById(R.id.value);
			ImageView contactLogo = (ImageView) vi.findViewById(R.id.logoImage);

			String contactString = contacts.get(type.getKey());

			contactValue.setText(contactString);

			contactLogo.setImageBitmap(BitmapFactory.decodeResource(getResources(), type.getDrawableResource()));
			vi.setTag(new Pair<CommunicationType, String>(type, contactString));

			vi.setOnClickListener(new OnClickListener()
			{
				@SuppressWarnings("unchecked")
				@Override
				public void onClick(View v)
				{
					Pair<CommunicationType, String> pair = (Pair<CommunicationType, String>) v.getTag();

					switch(pair.first)
					{
						case SKYPE:
							// TODO add alert dialog with offer to download skype from google play for free
							OffsideActivityHelper.initiateSkypeUri(UserDetailsActivity.this, "skype:" + pair.second);
							break;
						case FACEBOOK:
						case VKONTAKTE:
						case TWITTER:
							String url = pair.second;
							if(!url.startsWith("http://") && !url.startsWith("https://"))
								url = "http://" + url;
							Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
							startActivity(browser);
						default:
							break;
					}
				}
			});
			contactList.addView(vi);
		}
	}

	private void addRecentMessages(int[] msgIds)
	{
		List<Message> messages;

		try
		{
			List<Integer> iterable = new ArrayList<Integer>();
			for(int id : msgIds)
			{
				iterable.add(id);
			}
			messages = HelperFactory.getHelper().getMessageDAO().getByIds(iterable);
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			return;
		}

		LayoutInflater inflater = LayoutInflater.from(UserDetailsActivity.this);

		for(int i = 0; i < messages.size(); i++)
		{
			addMessageItem(messages.get(i), inflater);
		}
	}

	private void addMessageItem(Message message, LayoutInflater inflater)
	{
		View vi;
		// Bitmap ava;
		if(message.getDirection() == Direction.From)
		{
			vi = inflater.inflate(R.layout.item_message_to_simple, messageList, false);
			// ava = person.getImage();
		}
		else
		{
			vi = inflater.inflate(R.layout.item_message_from_simple, messageList, false);
			// ava = Profile.getInstance(this).getImage();
		}

		TextView messTextView = (TextView) vi.findViewById(R.id.message);
		// ImageView avatarImageView = (ImageView) vi.findViewById(R.id.image);

		messTextView.setText(message.getMessage());

		// avatarImageView.setImageBitmap(ava);

		// in future if message will be delivered after remote user send message to us,
		// we will popup our message to top(by delivery time reason). id then we will need.
		vi.setTag(message.getId());

		messageList.addView(vi, 0);
	}

	private class AsyncPersonLoader extends AsyncTask<String, Void, SocialUser>
	{

		@Override
		protected SocialUser doInBackground(String... params)
		{
			boolean contactsOnly = params.length > 1;

			SocialUser user = null;
			try
			{
				user = HelperFactory.getHelper().getSocialUserDAO().getUserByGid(params[0]);
			}
			catch(SQLException e)
			{
				Log.e("ily", "Exception", e);
			}

			if(contactsOnly)
			{
				// обнулением gid сигнализируем что хотим обновить только контакты.
				user.gid = null;
			}

			return user;
		}

		@Override
		protected void onPostExecute(SocialUser result)
		{
			super.onPostExecute(result);

			if(result != null)
			{
				// обнулением gid сигнализируем что хотим обновить только контакты.
				if(result.getGid() != null)
				{
					person = result;

					new AsyncMessageLoader().execute(person.gid);

					imageView.setImageBitmap(person.getImage());

					((TextView) findViewById(R.id.age)).setText(String.format(getString(R.string.format_age),
							Util.getAgeString(UserDetailsActivity.this, person.getAge())));

					nicknameTextView.setText(result.nick);
					statusTextView.setText(result.status);

					if(person.likeTo)
					{
						ivWinkOutgoing.setImageResource(R.drawable.wink_outgoing_done);
					}
					if(person.likeFrom)
					{
						ivWinkIncome.setImageResource(R.drawable.wink_income_done);
					}
				}

				inflateContacts(result.getContacts());

				mHandler.post(mRunnable);
			}
		}
	}

	private class AsyncMessageLoader extends AsyncTask<String, Void, List<Message>>
	{

		@Override
		protected List<Message> doInBackground(String... params)
		{
			List<Message> messages = null;
			try
			{
				messages = HelperFactory.getHelper().getMessageDAO().getByUserGid(params[0]);
			}
			catch(SQLException e)
			{
				Log.e("ily", "Exception", e);
			}

			return messages;
		}

		@Override
		protected void onPostExecute(List<Message> result)
		{
			super.onPostExecute(result);

			if(result != null)
			{
				LayoutInflater inflater = LayoutInflater.from(UserDetailsActivity.this);

				messageList.removeAllViews();

				for(int i = 0; i < result.size(); i++)
				{
					Message message = result.get(i);

					addMessageItem(message, inflater);
				}
			}
		}
	}

	public void onImageViewClick(View view)
	{

	}

}

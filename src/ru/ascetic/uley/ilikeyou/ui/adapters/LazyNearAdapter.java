﻿package ru.ascetic.uley.ilikeyou.ui.adapters;

import java.util.List;

import ru.ascetic.uley.ilikeyou.LikeYouApplication;
import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.readystatesoftware.viewbadger.BadgeView;

public class LazyNearAdapter extends BaseAdapter
{

	private Activity activity;
	private List<SocialUser> persons;
	private static LayoutInflater inflater = null;
	private Handler mHandler;

	private TranslateAnimation messageAnim;
	private Animation apperanceAnim;

	private final Runnable mRunnable = new Runnable()
	{
		public void run()
		{
			notifyDataSetChanged();

			mHandler.postDelayed(this, LikeYouApplication.SINCE_MAX / 12);
		}
	};

	public LazyNearAdapter(Activity a, List<SocialUser> items)
	{
		activity = a;
		this.persons = items;
		inflater = (LayoutInflater) activity.getLayoutInflater();

		messageAnim = new TranslateAnimation(1000, 0, 0, 0);
		messageAnim.setInterpolator(new BounceInterpolator());
		messageAnim.setDuration(1000);

		apperanceAnim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_left);

		mHandler = new Handler();
		mHandler.post(mRunnable);
	}

	public int getCount()
	{
		return persons.size();
	}

	public Object getItem(int position)
	{
		return persons.get(position);
	}

	public long getItemId(int position)
	{
		return position;
	}

	@SuppressLint("InflateParams")
	public View getView(int position, View convertView, ViewGroup parent)
	{
		SocialUser person = persons.get(position);

		long max_since = LikeYouApplication.SINCE_MAX;

		long elapsed = System.currentTimeMillis() - person.lastConnection;

		if(elapsed < 0 || elapsed > max_since)
		{
			convertView = inflater.inflate(R.layout.item_person_null, null);
			return convertView;
		}

		View vi = convertView;
		if(vi == null || vi.getTag() == null)
			vi = inflater.inflate(R.layout.item_person, null);

		ViewHolder holder = (ViewHolder) vi.getTag();

		// анимацию включаем только для новых контактов.
		boolean animationNeeded = holder == null;

		if(holder == null)
		{
			holder = new ViewHolder();
			holder.nick = (TextView) vi.findViewById(R.id.nickname);
			holder.age = (TextView) vi.findViewById(R.id.age);
			holder.since_duration = (TextView) vi.findViewById(R.id.since_duration);
			holder.avatar = (ImageView) vi.findViewById(R.id.avatar);
			holder.winkIncome = (ImageView) vi.findViewById(R.id.ivWinkIncome);
			holder.winkOutgoing = (ImageView) vi.findViewById(R.id.ivWinkOutgoing);
			vi.setTag(holder);
		}

		// Setting all values in listview
		holder.nick.setText(person.nick);
		holder.age.setText(String.format(activity.getString(R.string.format_age), person.getAge()));

		int color = Util.getNearColor(elapsed, activity.getResources().getColor(R.color.color_since));

		holder.since_duration.setTextColor(color);
		holder.since_duration.setText(activity.getString(R.string.near));

		if(person.likeTo)
		{
			holder.winkOutgoing.setImageResource(R.drawable.wink_outgoing_done);
		}
		else
		{
			holder.winkOutgoing.setImageResource(R.drawable.wink_outgoing_disabled);
		}

		if(person.likeFrom)
		{
			holder.winkIncome.setImageResource(R.drawable.wink_income_done);
		}
		else
		{
			holder.winkIncome.setImageResource(R.drawable.wink_income_disabled);
		}

		holder.avatar.setImageBitmap(person.getImage());

		int recentMessagesNum = 0;
		if(person.tag != null)
		{
			recentMessagesNum = (Integer) person.tag;
		}

		if(recentMessagesNum > 0)
		{
			if(holder.badge == null)
			{
				View view = (View) vi.findViewById(R.id.badgePlace);
				holder.badge = new BadgeView(activity, view);
				holder.badge.setBadgePosition(BadgeView.POSITION_BOTTOM_RIGHT);
			}

			// holder.badge.hide(true);
			holder.badge.setText(String.format(activity.getString(R.string.new_messages), String.valueOf(recentMessagesNum)));

			if(!holder.badge.isShown())
			{
				holder.badge.toggle(messageAnim, null);
			}
		}
		else if(holder.badge != null)
		{
			holder.badge.hide();
		}

		if(animationNeeded)
			vi.startAnimation(apperanceAnim);

		return vi;
	}

	class ViewHolder
	{
		public TextView nick;
		public TextView age;
		public TextView since_duration;
		public ImageView avatar;
		public ImageView winkIncome;
		public ImageView winkOutgoing;
		public BadgeView badge;
	}
}
﻿package ru.ascetic.uley.ilikeyou.ui.adapters;

import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.io.ImageLoader;
import ru.ascetic.uley.ilikeyou.objects.Profile.Contact;
import ru.ascetic.uley.ilikeyou.ui.views.SegmentedRadioGroup;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class LazyContactsAdapter extends BaseAdapter
{
	private Activity activity;
	private List<Contact> data;
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public LazyContactsAdapter(Activity a, List<Contact> d)
	{
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount()
	{
		return data.size();
	}

	public Object getItem(int position)
	{
		return data.get(position);
	}

	public long getItemId(int position)
	{
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		View vi = convertView;
		if(convertView == null)
			vi = inflater.inflate(R.layout.item_person, parent, false);

		// TextView contactName = (TextView) vi.findViewById(R.id.name);
		TextView contactValue = (TextView) vi.findViewById(R.id.value);
		TextView visibilityTextView = (TextView) vi.findViewById(R.id.txt_visibility);

		SegmentedRadioGroup group = (SegmentedRadioGroup) vi.findViewById(R.id.radio_visibility);
		ImageView contactLogo = (ImageView) vi.findViewById(R.id.logoImage);

		Contact contact = data.get(position);

		// TODO: вынести в локализацию!
		String visibility = "";
		switch(contact.visibility)
		{
			case Invisible:
				visibility = "Скрыто";
				group.check(R.id.contact_invisible);
				break;
			case Semivisible:
				visibility = "Для избранных";
				group.check(R.id.contact_semivisible);// R.id.button_c_semi_visible
				break;
			case Visible:
				visibility = "Видно всем";
				group.check(R.id.contact_visible);
				break;
			default:
				break;
		}

		visibilityTextView.setText(visibility);
		// contactName.setText(activity.getString(contact.type.getStringResource()));
		contactValue.setText(contact.value);
		contactLogo.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), contact.type.getDrawableResource()));

		return vi;
	}
}
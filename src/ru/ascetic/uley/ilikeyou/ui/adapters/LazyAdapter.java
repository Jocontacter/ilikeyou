package ru.ascetic.uley.ilikeyou.ui.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.readystatesoftware.viewbadger.BadgeView;

public class LazyAdapter extends BaseAdapter
{

	private Activity activity;
	private List<SocialUser> persons;
	private static LayoutInflater inflater = null;

	public LazyAdapter(Activity a, List<SocialUser> items)
	{
		activity = a;
		this.persons = items;
		inflater = (LayoutInflater) activity.getLayoutInflater();
	}

	public int getCount()
	{
		return persons.size();
	}

	public Object getItem(int position)
	{
		return persons.get(position);
	}

	public long getItemId(int position)
	{
		return position;
	}

	@SuppressLint("InflateParams")
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View vi = convertView;
		if(convertView == null)
			vi = inflater.inflate(R.layout.item_person_history, null);

		ViewHolder holder = (ViewHolder) vi.getTag();

		if(holder == null)
		{
			holder = new ViewHolder();
			holder.nick = (TextView) vi.findViewById(R.id.nickname);
			holder.age = (TextView) vi.findViewById(R.id.age);
			holder.since_duration = (TextView) vi.findViewById(R.id.since_duration);
			holder.avatar = (ImageView) vi.findViewById(R.id.avatar);
			holder.winkIncome = (ImageView) vi.findViewById(R.id.ivWinkIncome);
			holder.winkOutgoing = (ImageView) vi.findViewById(R.id.ivWinkOutgoing);
			vi.setTag(holder);
		}

		SocialUser person = persons.get(position);

		// Setting all values in listview
		holder.nick.setText(person.nick);
		holder.age.setText(String.format(activity.getString(R.string.format_age), person.getAge()));

		if(person.likeTo)
		{
			holder.winkOutgoing.setImageResource(R.drawable.wink_outgoing_done);
		}
		else
		{
			holder.winkOutgoing.setImageResource(R.drawable.wink_outgoing_disabled);
		}

		if(person.likeFrom)
		{
			holder.winkIncome.setImageResource(R.drawable.wink_income_done);
		}
		else
		{
			holder.winkIncome.setImageResource(R.drawable.wink_income_disabled);
		}

		holder.since_duration.setText(getLastContactDateString(person.lastConnection));

		holder.avatar.setImageBitmap(person.getImage());

		return vi;
	}

	private String getLastContactDateString(long time)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		String template;

		if(calendar.getTimeInMillis() <= time)
		{
			template = activity.getString(R.string.template_contact_time_short);
		}
		else
		{
			template = activity.getString(R.string.template_contact_time);
		}

		DateFormat formatter = new SimpleDateFormat(template, activity.getResources().getConfiguration().locale);

		calendar.setTimeInMillis(time);

		return String.format(activity.getString(R.string.format_last_contact), formatter.format(calendar.getTime()));
	}

	class ViewHolder
	{
		public TextView nick;
		public TextView age;
		public TextView since_duration;
		public ImageView avatar;
		public ImageView winkIncome;
		public ImageView winkOutgoing;
		public BadgeView badge;
	}
}
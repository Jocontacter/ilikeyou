package ru.ascetic.uley.ilikeyou;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.vk.sdk.util.VKUtil;

public class DebugUtil
{

	public static void logFbKeyhash(Context context)
	{
		// Add code to print out the key hash
		try
		{
			PackageInfo info = context.getPackageManager().getPackageInfo("ru.ascetic.uley.ilikeyou", PackageManager.GET_SIGNATURES);
			for(Signature signature : info.signatures)
			{
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("FB Keyhash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		}
		catch(NameNotFoundException e)
		{

		}
		catch(NoSuchAlgorithmException e)
		{

		}
	}

	public static void logVkFingerprint(Context context)
	{
		String[] fingerprint = VKUtil.getCertificateFingerprint(context, context.getPackageName());
		Log.d("VK Fingerprint", fingerprint[0]);
	}

}

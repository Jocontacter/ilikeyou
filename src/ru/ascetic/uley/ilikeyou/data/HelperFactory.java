﻿/**
 * 
 **/
package ru.ascetic.uley.ilikeyou.data;

import ru.ascetic.uley.ilikeyou.data.dao.DatabaseHelper;
import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * @author Evgeniy
 * 
 **/
public class HelperFactory extends ru.ascetic.uley.data.HelperFactory
{
	private static DatabaseHelper databaseHelper;

	public static DatabaseHelper getHelper()
	{
		return databaseHelper;
	}

	public static void setHelper(Context context)
	{
		databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
	}

	public static void releaseHelper()
	{
		OpenHelperManager.releaseHelper();
		databaseHelper = null;
	}
}

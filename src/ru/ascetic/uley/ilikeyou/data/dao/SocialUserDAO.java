﻿/**
 * 
 **/
package ru.ascetic.uley.ilikeyou.data.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.ascetic.uley.ilikeyou.LikeYouApplication;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

/**
 * @author Evgeniy
 * 
 **/
public class SocialUserDAO extends BaseDaoImpl<SocialUser, String>
{
	protected SocialUserDAO(ConnectionSource connectionSource, Class<SocialUser> dataClass) throws SQLException
	{
		super(connectionSource, dataClass);
	}

	public List<SocialUser> getNearUsers() throws SQLException
	{
		QueryBuilder<SocialUser, String> queryBuilder = queryBuilder();
		queryBuilder.where().gt("lastConnection", System.currentTimeMillis() - LikeYouApplication.SINCE_MAX);

		queryBuilder.orderBy("lastConnection", false);

		PreparedQuery<SocialUser> preparedQuery = queryBuilder.prepare();
		return query(preparedQuery);
	}

	public List<SocialUser> getAllUsers() throws SQLException
	{
		QueryBuilder<SocialUser, String> queryBuilder = queryBuilder();

		queryBuilder.orderBy("lastConnection", false);
		queryBuilder.limit(20L);

		PreparedQuery<SocialUser> preparedQuery = queryBuilder.prepare();
		return query(preparedQuery);
	}

	public List<String> getOutgoingLikeUsers() throws SQLException
	{
		return getLikeUsers(false, true);
	}

	public List<String> getIncomingLikeUsers() throws SQLException
	{
		return getLikeUsers(true, false);
	}

	private List<String> getLikeUsers(boolean incoming, boolean outgoing) throws SQLException
	{
		List<String> result = new ArrayList<String>();

		if(!incoming && !outgoing)
		{
			return result;
		}

		QueryBuilder<SocialUser, String> queryBuilder = queryBuilder();
		queryBuilder.selectColumns("gid");

		if(outgoing)
			queryBuilder.where().eq("likeTo", outgoing);
		if(incoming)
			queryBuilder.where().eq("likeFrom", incoming);

		PreparedQuery<SocialUser> preparedQuery = queryBuilder.prepare();
		List<SocialUser> userList = query(preparedQuery);

		for(SocialUser user : userList)
		{
			result.add(user.gid);
		}

		return result;
	}

	public SocialUser getUserByNick(String nick) throws SQLException
	{
		QueryBuilder<SocialUser, String> queryBuilder = queryBuilder();
		queryBuilder.where().eq("nick", nick);
		PreparedQuery<SocialUser> preparedQuery = queryBuilder.prepare();
		List<SocialUser> userList = query(preparedQuery);
		if(userList.size() > 0)
			return userList.get(0);
		else
			return null;
	}

	public SocialUser getUserByGid(String gid) throws SQLException
	{
		QueryBuilder<SocialUser, String> queryBuilder = queryBuilder();
		queryBuilder.where().eq("gid", gid);
		PreparedQuery<SocialUser> preparedQuery = queryBuilder.prepare();
		List<SocialUser> userList = query(preparedQuery);
		if(userList.size() > 0)
			return userList.get(0);
		else
			return null;
	}

	/**
	 * Updates fields: lastConnection, local_digest, likeTo, likeFrom
	 **/
	public void updateLightweightFields(SocialUser user) throws SQLException
	{
		UpdateBuilder<SocialUser, String> updateBuilder = updateBuilder();
		updateBuilder.updateColumnValue("lastConnection", user.lastConnection);
		updateBuilder.updateColumnValue("local_digest", user.local_digest);
		updateBuilder.updateColumnValue("likeTo", user.likeTo);
		updateBuilder.updateColumnValue("likeFrom", user.likeFrom);
		updateBuilder.where().eq("gid", user.gid);
		update(updateBuilder.prepare());
	}
}

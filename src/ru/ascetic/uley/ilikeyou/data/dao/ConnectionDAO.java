﻿/**
 * 
 **/
package ru.ascetic.uley.ilikeyou.data.dao;

import java.sql.SQLException;
import java.util.List;

import ru.ascetic.uley.ilikeyou.objects.Connection;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

/**
 * @author Evgeniy
 *
 **/
public class ConnectionDAO extends BaseDaoImpl<Connection, String>
{
	protected ConnectionDAO(ConnectionSource connectionSource, Class<Connection> dataClass) throws SQLException
	{
		super(connectionSource, dataClass);
	}
	
	public Connection getByGid(String gid) throws SQLException
	{
		QueryBuilder<Connection, String> queryBuilder = queryBuilder();
		queryBuilder.where().eq("remote_gid", gid);
		PreparedQuery<Connection> preparedQuery = queryBuilder.prepare();
		List<Connection> conList = query(preparedQuery);
		if(conList.size()>0)
			return conList.get(0);
		else return null;
	}
}

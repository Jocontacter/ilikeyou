﻿/**
 * 
 **/
package ru.ascetic.uley.ilikeyou.data.dao;

import java.sql.SQLException;
import java.util.List;

import ru.ascetic.uley.ilikeyou.objects.Message;
import ru.ascetic.uley.ilikeyou.objects.Message.Direction;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

/**
 * @author Evgeniy
 *
 **/
public class MessageDAO extends BaseDaoImpl<Message, Integer>
{
	protected MessageDAO(ConnectionSource connectionSource, Class<Message> dataClass) throws SQLException
	{
		super(connectionSource, dataClass);
	}
	
	/** Возвращает всю переписку с указанным пользователем **/
	public List<Message> getByUserGid(String gid) throws SQLException
	{
		QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
		queryBuilder.where().eq("remote_gid", gid);
		queryBuilder.orderBy("id", true);
		PreparedQuery<Message> preparedQuery = queryBuilder.prepare();
		List<Message> mList = query(preparedQuery);
		return mList;
	}
	
	/** Возвращает сообщения с указанными id **/
	public List<Message> getByIds(List<Integer> ids) throws SQLException
	{
		QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
		queryBuilder.where().in("id", ids);
		queryBuilder.orderBy("id", true);
		PreparedQuery<Message> preparedQuery = queryBuilder.prepare();
		List<Message> mList = query(preparedQuery);
		return mList;
	}
	
	/** Возвращает id сообщений, принятых от указанного пользователя **/
	public int[] getFromRemoteIds(String gid) throws SQLException
	{
		QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
		queryBuilder.selectColumns("remoteMessageId");
		queryBuilder.where()
			.eq("remote_gid", gid)
			.and().ne("remoteMessageId", 0);
		List<Message> mList = query(queryBuilder.prepare());
		
		int[] result = new int[mList.size()];
		
		int i=0;
		for(Message m: mList)
		{
			result[i] = m.getRemoteMessageId();
			i++;
		}
		
		return result;
	}
	
	/** Возвращает недоставленные сообщения **/
	public List<Message> getUnsent(String gid) throws SQLException
	{
		QueryBuilder<Message, Integer> queryBuilder = queryBuilder();
		queryBuilder.where()
			.eq("remote_gid", gid)
			.and().eq("direction", Direction.From)
			.and().eq("delivered", 0);
		
		queryBuilder.orderBy("id", true);
		
		List<Message> mList = query(queryBuilder.prepare());
		return mList;
	}
	
	public void updateDeliveredTime(int id) throws SQLException
	{
		UpdateBuilder<Message, Integer> updateBuilder = updateBuilder();
		updateBuilder.updateColumnValue("delivered", System.currentTimeMillis());
		updateBuilder.where().eq("id", id);
		update(updateBuilder.prepare());
	}
}

﻿package ru.ascetic.uley.ilikeyou.data.dao;

import java.sql.SQLException;

import ru.ascetic.uley.ilikeyou.objects.Connection;
import ru.ascetic.uley.ilikeyou.objects.Message;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends ru.ascetic.uley.data.dao.DatabaseHelper
{
	private static final String TAG = DatabaseHelper.class.getSimpleName();

	// имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
	private static final String DATABASE_NAME = "ilikeyoutest15.db";

	// с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод
	// onUpgrade();
	private static final int DATABASE_VERSION = 1;

	// ссылки на DAO соответсвующие сущностям, хранимым в БД
	private SocialUserDAO socialUserDAO = null;
	private ConnectionDAO connectionDAO = null;
	private MessageDAO messageDAO = null;

	public DatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, DATABASE_VERSION);// , R.raw.ormlite_config
	}

	// Выполняется, когда файл с БД не найден на устройстве
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource)
	{
		super.onCreate(db, connectionSource);

		try
		{
			TableUtils.createTable(connectionSource, SocialUser.class);
			TableUtils.createTable(connectionSource, Connection.class);
			TableUtils.createTable(connectionSource, Message.class);
		}
		catch(SQLException e)
		{
			Log.e(TAG, "error creating DB " + DATABASE_NAME);
			throw new RuntimeException(e);
		}
	}

	// Выполняется, когда БД имеет версию отличную от текущей
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer)
	{
		super.onUpgrade(db, connectionSource, oldVer, newVer);

		try
		{
			// Так делают ленивые, гораздо предпочтительнее не удаляя БД аккуратно вносить изменения
			TableUtils.dropTable(connectionSource, SocialUser.class, true);
			TableUtils.dropTable(connectionSource, Connection.class, true);
			TableUtils.dropTable(connectionSource, Message.class, true);
			onCreate(db, connectionSource);
		}
		catch(SQLException e)
		{
			Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
			throw new RuntimeException(e);
		}
	}

	public SocialUserDAO getSocialUserDAO() throws SQLException
	{
		if(socialUserDAO == null)
		{
			socialUserDAO = new SocialUserDAO(getConnectionSource(), SocialUser.class);
		}
		return socialUserDAO;
	}

	public ConnectionDAO getConnectionDAO() throws SQLException
	{
		if(connectionDAO == null)
		{
			connectionDAO = new ConnectionDAO(getConnectionSource(), Connection.class);
		}
		return connectionDAO;
	}

	public MessageDAO getMessageDAO() throws SQLException
	{
		if(messageDAO == null)
		{
			messageDAO = new MessageDAO(getConnectionSource(), Message.class);
		}
		return messageDAO;
	}

	// выполняется при закрытии приложения
	@Override
	public void close()
	{
		super.close();

		socialUserDAO = null;
		connectionDAO = null;
		messageDAO = null;
	}
}

﻿package ru.ascetic.uley.ilikeyou.data;

import ru.ascetic.uley.ilikeyou.objects.SocialUser;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil
{
	private static final Class<?>[] classes = new Class[]{ SocialUser.class };
	public static void main(String[] args) throws Exception
	{
		writeConfigFile("ormlite_config.txt", classes);
	}
	public DatabaseConfigUtil()
	{
	}

}

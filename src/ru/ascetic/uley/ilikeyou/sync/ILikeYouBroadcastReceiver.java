﻿package ru.ascetic.uley.ilikeyou.sync;

import android.content.Context;
import android.content.Intent;
import ru.ascetic.uley.api.UleyClientApplicationDataProvider;
import ru.ascetic.uley.api.UleyClientBroadcastReceiver;

public class ILikeYouBroadcastReceiver extends UleyClientBroadcastReceiver
{
	public ILikeYouBroadcastReceiver()
	{
		super(new UleyDataProvider());
	}

	public ILikeYouBroadcastReceiver(UleyClientApplicationDataProvider provider)
	{
		super(provider);
	}
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		ReceiveHandler handler = new ReceiveHandler(context, intent);
		Thread thread = new Thread(handler);
		thread.setName("ReceiveHandler");
		thread.start();
	}
	
	private class ReceiveHandler implements Runnable
	{
		private Context context;
		private Intent intent;
		
		public ReceiveHandler(Context context, Intent intent)
		{
			this.context = context;
			this.intent = intent;
		}

		@Override
		public void run()
		{
			ILikeYouBroadcastReceiver.super.onReceive(context, intent);
		}
	}
}

﻿/**
 * 
 */
package ru.ascetic.uley.ilikeyou.sync;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import ru.ascetic.uley.api.UleyClientApplicationDataProvider;
import ru.ascetic.uley.api.UleyClientBroadcastReceiver;
import ru.ascetic.uley.api.UleyData;
import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.DataPacket;
import ru.ascetic.uley.ilikeyou.objects.Message;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.SocialUser;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;

/**
 * @author EStepanov
 * 
 */
public class UleyDataProvider implements UleyClientApplicationDataProvider
{
	private Context context;
	public final static String PERSON_RECEIVED = "ru.ascetic.uley.ilikeyou.PERSON_RECEIVED";
	public final static String MESSAGES_RECEIVED = "ru.ascetic.uley.ilikeyou.MESSAGES_RECEIVED";

	@Override
	public void setContext(Context context)
	{
		this.context = context;
	}

	/*
	 * (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getAppName()
	 */
	@Override
	public String getAppName()
	{
		return context.getString(R.string.app_name);
	}

	/*
	 * (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getStatFormatString()
	 */
	@Override
	public String getStatFormatString()
	{
		return context.getString(R.string.stat_format_string);
	}

	/*
	 * (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getGetDataActionString()
	 */
	@Override
	public String getGetDataActionString()
	{
		return "ru.ascetic.uley.ilikeyou.GET_DATA";
	}

	/*
	 * (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getReceiveDataActionString()
	 */
	@Override
	public String getReceiveDataActionString()
	{
		return "ru.ascetic.uley.ilikeyou.RECEIVE_DATA";
	}

	/*
	 * get user profile gid for check on another devices (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getSyncData()
	 */
	@Override
	public byte[] getSyncData()
	{
		// showAppNotification(context, "iLikeYou: Sync-packet requested");

		Profile profile = Profile.getInstance(context);
		String result = profile.getSyncData();

		try
		{
			return result.getBytes("UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			throw new RuntimeException(e);
		}
	}

	/*
	 * There send things like user data, 'likes' and messages Speciality of this method is in syncdata, which are sent
	 * common for all target devices from sender. Therefore, we cant to receive sync data destined especially for us.
	 * (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#getData(byte[])
	 */
	@Override
	public UleyData getData(byte[] syncdata)
	{
		byte[] dummyData = new byte[] {};
		UleyData dummyResult = new UleyData(dummyData, null, 0);

		try
		{
			String stringSyncData = new String(syncdata, "UTF-8");

			if(stringSyncData.equals("none"))
			{
				Log.d("UleyDataProvider", "none received as sync data at getData()");
				// showAppNotification(context, "none received as sync data");
				return dummyResult;
			}
			else
			{
				JSONObject personJsonGid = new JSONObject(stringSyncData);
				String personGid = personJsonGid.getString("gid");

				// connection info updates inside DataPacket.create()!
				// stringSyncData.contains - i don't want to parse json - it will be redundant there - just search our
				// gid in all string
				DataPacket packet = DataPacket.create(Profile.getInstance(context), personGid,
						stringSyncData.contains(Profile.getInstance(context).gid));

				showAppNotification(context, "iLikeYou: Data-packet" + (packet.getRemoteUser() == null ? "" : " with person") + " sent");

				return new UleyData(packet.createPacket().getBytes("UTF-8"), null, 1);
			}
		}
		catch(UnsupportedEncodingException e)
		{
			throw new RuntimeException(e);
		}
		catch(JSONException e)
		{
			throw new RuntimeException(e);
		}
	}

	/*
	 * receive likes, messages, user data and so on.. (non-Javadoc)
	 * @see ru.ascetic.uley.api.UleyClientApplicationDataProvider#receiveData(byte[])
	 */
	@Override
	public void receiveData(byte[] data)
	{
		String dataString;

		try
		{
			dataString = new String(data, "UTF-8");
		}
		catch(UnsupportedEncodingException e)
		{
			Log.e("ily", "Fatal exception", e);
			return;
		}

		if(dataString.equals("none"))
		{
			Log.d("UleyDataProvider", "none received as data at receiveData()");
			// showAppNotification(context, "none received as data");
			return;
		}

		long connectionTime = System.currentTimeMillis();

		DataPacket packet = DataPacket.parse(dataString);

		SocialUser incomingPerson = packet.getRemoteUser();

		// showAppNotification(context, "iLikeYou: Data-packet delivered" + (incomingPerson == null ? "" :
		// "(with person)"));

		List<Message> messages = packet.getMessages();
		int[] mess_ids = new int[messages.size()];

		for(int i = 0; i < messages.size(); i++)
		{
			mess_ids[i] = messages.get(i).getId();
		}

		Intent personIntent = new Intent(PERSON_RECEIVED);

		boolean isIncomingLikeReceived = false;
		boolean isOutgoingLikeReceivedOnRemote = false;
		boolean personIsChangedOrNew = incomingPerson != null;

		// if null - then remote device supposed, what we already have a relevant version of person data.
		if(!personIsChangedOrNew)
		{
			// If person data was not delivered - check for presence it in our db.
			// If not exist or our data is not relevant - add to "mandatory to deliver" list.
			try
			{
				incomingPerson = HelperFactory.getHelper().getSocialUserDAO().getUserByGid(packet.getGid());
			}
			catch(SQLException e)
			{
				Log.e("ily", "Exception", e);
				return;
			}

			boolean inconsistency = true;
			if(incomingPerson != null)
				inconsistency = !incomingPerson.getDigest().equals(packet.getDigest());

			if(inconsistency)
			{
				Log.w("ily", "Inconsistency!");

				Profile.getInstance(context).needsToUpdate.add(packet.getGid());
				UleyClientBroadcastReceiver.sendClientInfo(context, UleyDataProvider.this);

				// undelivered incoming person does not exist in our db - nothing to update. return.
				// otherwise - continue and sent broadcast to UI for person list updating
				if(incomingPerson == null)
				{
					Log.w("ily", "non delivered person not exists in db!");
					return;
				}
			}
			else
			{
				// in case when we are already have a relevant version of person data - just update a time in UI
				personIntent.putExtra("event", "updateTime");
			}
		}
		else
		{
			// probably, person data was sent to us through needToUpdate list. In another case - it is unknown
			// user.
			// just in case try to delete gid from "mandatory to deliver" list
			if(Profile.getInstance(context).needsToUpdate.remove(packet.getGid()))
			{
				UleyClientBroadcastReceiver.sendClientInfo(context, UleyDataProvider.this);
			}
		}

		if(packet.getLikeTo())
		{
			incomingPerson.likeFrom = true;

			if(Profile.getInstance(context).incomingLike(packet.getGid()))
			{
				// был ли принят ли входящий лайк
				isIncomingLikeReceived = true;
			}
		}

		if(packet.getLikeFrom())
		{
			incomingPerson.likeTo = true;

			if(Profile.getInstance(context).addDeliveredOutgoingLike(packet.getGid()))
			{
				// был ли принят на remote исходящий лайк
				isOutgoingLikeReceivedOnRemote = true;
			}
		}

		// time update
		incomingPerson.lastConnection = connectionTime;

		if(incomingPerson.local_digest == null)
		{
			// Only for new contacts assigning there. Update take place in DataPacket.create()
			// There we have no guarantees, what our packet(given in getData) will be delivered to remote.
			// but we updates a local_digest for incoming person..
			// in case, when our person data was not delivered to remote -
			// digest in the next packets, all the same, will be checked on remote and everything will be ok
			incomingPerson.local_digest = Profile.getInstance(context).getDigest();
		}

		CreateOrUpdateStatus status;

		try
		{
			status = HelperFactory.getHelper().getSocialUserDAO().createOrUpdate(incomingPerson);
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			return;
		}

		LocalBroadcastManager localBroadcaster = LocalBroadcastManager.getInstance(context);

		{
			Intent messagesIntent = new Intent(MESSAGES_RECEIVED);
			messagesIntent.putExtra("gid", packet.getGid());
			messagesIntent.putExtra("message_ids", mess_ids);
			messagesIntent.putExtra("is_changed", personIsChangedOrNew);
			messagesIntent.putExtra("is_like_received", isIncomingLikeReceived);
			messagesIntent.putExtra("is_like_received_on_remote", isOutgoingLikeReceivedOnRemote);
			localBroadcaster.sendBroadcast(messagesIntent);
		}

		personIntent.putExtra("gid", packet.getGid());
		personIntent.putExtra("event", (status.isCreated() ? "add" : "update"));

		if(status.isUpdated())
		{
			personIntent.putExtra("message_ids", mess_ids);
			personIntent.putExtra("is_like_received", isIncomingLikeReceived);
			personIntent.putExtra("is_like_received_on_remote", isOutgoingLikeReceivedOnRemote);
		}

		localBroadcaster.sendBroadcast(personIntent);
	}

	/**
	 * The notification is the icon and associated expanded entry in the status bar.
	 */
	void showAppNotification(Context context, String message)
	{
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification noti = new NotificationCompat.Builder(context).setContentTitle("new uley message").setContentText(message)
				.setSmallIcon(android.R.drawable.ic_menu_send).build();

		nm.notify(238452034, noti);
	}

}

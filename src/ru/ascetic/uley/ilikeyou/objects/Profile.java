﻿/**
 * 
 */
package ru.ascetic.uley.ilikeyou.objects;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

/**
 * @author EStepanov
 * 
 */
public class Profile extends SocialUser
{
	private SharedPreferences prefs;
	private static Profile instance = null;

	public static Profile getInstance(Context context)
	{
		if(instance == null)
		{
			instance = new Profile();
			instance.prefs = context.getSharedPreferences("Profile", Context.MODE_PRIVATE);
			instance.load();
		}

		return instance;
	}

	private HashMap<String, ContactVisibility> privacy = new HashMap<String, ContactVisibility>();
	private List<String> incomingLikes = new ArrayList<String>();
	private List<String> outgoingLikes = new ArrayList<String>();

	@Override
	protected void updateDigest()
	{
		String vis = "";

		for(CommunicationType type : CommunicationType.values())
		{
			String key = type.getKey();

			if(contacts.containsKey(key))
			{
				vis += key + "=" + contacts.get(key) + ":" + privacy.get(key).name() + ";";
			}
		}

		super.updateDigest(vis);
	}

	/**
	 * Содержит gid'ы пользователей, от которых требуется насильная передача профайла, даже если у них значится что
	 * данному пользователю профайл был передан
	 **/
	public List<String> needsToUpdate = new ArrayList<String>();

	public void load()
	{
		String serializedUser = prefs.getString("profile", "none");
		if(serializedUser.equals("none"))
		{
			this.gid = UUID.randomUUID().toString();
			saveGeneral();
			saveContacts();
		}
		else
		{
			String serializedContacts = prefs.getString("profileContacts", null);
			String base64Image = prefs.getString("profileImage", null);
			deserialize(serializedUser);
			try
			{
				deserializeContacts(serializedContacts);
				if(base64Image != null)
					imageBytes = Base64.decode(base64Image, Base64.DEFAULT);
			}
			catch(JSONException e)
			{
				Log.e("ily", "Exception", e);

				throw new RuntimeException(e);
			}

			try
			{
				incomingLikes = HelperFactory.getHelper().getSocialUserDAO().getIncomingLikeUsers();
				outgoingLikes = HelperFactory.getHelper().getSocialUserDAO().getOutgoingLikeUsers();
			}
			catch(SQLException e)
			{
				Log.e("Profile.load()", "Exception", e);
			}

			updateDigest();
		}
	}

	/**
	 * <p>
	 * Cохраняет общие поля, изображение пользователя и его контакты.
	 * </p>
	 * <p>
	 * <b>Накладная операция - если нет твердой необходимости сохранять все - используйте {@link #saveGeneral()},
	 * {@link #saveContacts()} и {@link #saveImage()} по-отдельности</b>
	 * </p>
	 **/
	public void saveAll()
	{
		saveGeneral();
		saveContacts();
		saveImage();
	}

	/**
	 * Сохраняет только данные пользователя. Не сохраняет изображение и контактные данные с уровнями приватности
	 **/
	public void saveGeneral()
	{
		setDigest(null);

		String serialized;
		try
		{
			serialized = serialize(true, false);
		}
		catch(JSONException e)
		{
			Log.e("ily", "Exception", e);

			throw new RuntimeException(e);
		}

		prefs.edit().putString("profile", serialized).commit();
	}

	/**
	 * Сохраняет контактные данные профайла с уровнями приватности
	 **/
	public void saveContacts()
	{
		setDigest(null);

		JSONObject contactsJson;
		try
		{
			contactsJson = serializeContacts(true, false);
		}
		catch(JSONException e)
		{
			Log.e("ily", "Exception", e);

			throw new RuntimeException(e);
		}

		prefs.edit().putString("profileContacts", contactsJson.toString()).commit();
	}

	/**
	 * Сохраняет изображение профайла.
	 **/
	public void saveImage()
	{
		setDigest(null);

		if(imageBytes != null)
			prefs.edit().putString("profileImage", Base64.encodeToString(imageBytes, Base64.DEFAULT)).commit();
	}

	private void deserializeContacts(String serializedContacts) throws JSONException
	{
		JSONObject contactsJson = new JSONObject(serializedContacts);

		JSONObject privacyObj = contactsJson.getJSONObject("privacy");
		for(CommunicationType type : CommunicationType.values())
		{
			String key = type.getKey();
			if(privacyObj.has(key))
			{
				privacy.put(key, ContactVisibility.valueOf(privacyObj.optString(key)));
			}
		}

		JSONObject contactsObj = contactsJson.getJSONObject("contacts");
		for(CommunicationType type : CommunicationType.values())
		{
			String key = type.getKey();
			if(privacy.containsKey(key))
			{
				contacts.put(key, contactsObj.getString(key));
			}
		}
	}

	/**
	 * Сериализация профайла для передачи
	 * 
	 * @param isFavorite
	 *            - передача избранному пользователю или нет. Учавствует в фильтрации контактных данных
	 **/
	public String serializeForTransfer(boolean isFavorite)
	{
		try
		{
			return serialize(false, isFavorite);
		}
		catch(JSONException e)
		{
			Log.e("ily", "Exception", e);

			throw new RuntimeException(e);
		}
	}

	/**
	 * Сериализует данные профайла При forSave == true не сериализует: изображение, контакты, приватность При forSave ==
	 * false не сериализует приватность
	 * 
	 * @param forSave
	 *            контекст вызова метода(сохранение профайла/передача)
	 * @param isFavorite
	 *            В случае forSave==false - определяет фильтрацию списка контактов для передачи. В случае forSave ==
	 *            true не имеет значения
	 * @return сериализованный объект профайла
	 * @throws JSONException
	 **/
	private String serialize(boolean forSave, boolean isFavorite) throws JSONException
	{
		JSONObject result = null;
		try
		{
			result = new JSONObject();
			result.put("nick", nick);
			result.put("birthday", birthday);
			result.put("gid", gid);
			result.put("sex", sex.name());
			result.put("status", status);

			if(!forSave)
			{
				updateDigest();// IMPORTANT FOR GETTING ACTUAL DIGEST!
				result.put("digest", getDigest());

				if(imageBytes != null)
					result.put("image", Base64.encodeToString(imageBytes, Base64.DEFAULT));

				JSONObject contactsJson = serializeContacts(forSave, isFavorite);
				result.put("contacts", contactsJson.get("contacts"));
			}
		}
		catch(JSONException e)
		{
			throw new RuntimeException(e);
		}

		return result.toString();
	}

	private JSONObject serializeContacts(boolean forSave, boolean isFavorite) throws JSONException
	{
		JSONObject resultObj = new JSONObject();
		JSONObject contactsObj = new JSONObject();

		for(CommunicationType type : CommunicationType.values())
		{
			String key = type.getKey();

			if(privacy.containsKey(key))
			{
				if(!contacts.containsKey(key))
					throw new IllegalStateException("Inconsistence with storing of contacts and privacy key values!");

				if(forSave || privacy.get(key).equals(ContactVisibility.Visible)
						|| (privacy.get(key).equals(ContactVisibility.Semivisible) && isFavorite))
				{
					contactsObj.putOpt(key, contacts.get(key));
				}
			}
			else
			{
				if(contacts.containsKey(key))
					throw new IllegalStateException("Inconsistence with storing of contacts and privacy key values!");
			}
		}

		resultObj.put("contacts", contactsObj);

		if(forSave)
		{
			JSONObject privacyObj = new JSONObject();
			for(CommunicationType type : CommunicationType.values())
			{
				String key = type.getKey();
				if(privacy.containsKey(key))
				{
					privacyObj.put(key, privacy.get(key).name());
				}
			}

			resultObj.put("privacy", privacyObj);
		}

		return resultObj;
	}

	public String getSyncData()
	{
		JSONObject base = new JSONObject();

		try
		{
			base.put("gid", gid);
			for(String rem_gid : needsToUpdate)
			{
				base.accumulate("needsToUpdate", rem_gid);
			}
		}
		catch(JSONException e)
		{
			throw new RuntimeException(e);
		}

		return base.toString();
	}

	public List<String> getIncomingLikes()
	{
		return incomingLikes;
	}

	public List<String> getOutgoingLikes()
	{
		return outgoingLikes;
	}

	/**
	 * Входящий лайк
	 * 
	 * не обновляет базу данных - только память объекта!
	 * 
	 * @return true если информация о лайке новая для профиля. Иначе false
	 **/
	public boolean incomingLike(String gid)
	{
		if(!incomingLikes.contains(gid))
		{
			incomingLikes.add(gid);
			return true;
		}

		return false;
	}

	/**
	 * Регистрация доставки исходящего лайка
	 * 
	 * не обновляет базу данных - только память объекта!
	 * 
	 * @return true если информация о доставке новая для профиля. Иначе false
	 **/
	public boolean addDeliveredOutgoingLike(String gid)
	{
		if(!outgoingLikes.contains(gid))
		{
			outgoingLikes.add(gid);
			return true;
		}

		return false;
	}

	public boolean isValid()
	{
		// TODO: to check!
		return !(gid == null || gid.equals("") || nick == null || nick.equals(""));// || age==0
	}

	public ContactVisibility getVisibility(CommunicationType type)
	{
		return privacy.get(type.getKey());
	}

	public void setVisibility(CommunicationType type, ContactVisibility visibility)
	{
		if(!contacts.containsKey(type.getKey()))
			throw new IllegalArgumentException("Profile contacts does not contain these contact: " + type.getKey());

		privacy.put(type.getKey(), visibility);
	}

	public List<Contact> getProfileContacts()
	{
		return Contact.convert(contacts, privacy);
	}

	public void putContact(Contact contact)
	{
		contacts.put(contact.type.getKey(), contact.value);
		privacy.put(contact.type.getKey(), contact.visibility);
	}

	public void removeContact(CommunicationType type)
	{
		contacts.remove(type.getKey());
		privacy.remove(type.getKey());
	}

	public static class Contact
	{
		public CommunicationType type;
		public String value;
		public ContactVisibility visibility;

		public Contact()
		{
		}

		public Contact(CommunicationType type, String value, ContactVisibility visibility)
		{
			this.type = type;
			this.value = value;
			this.visibility = visibility;
		}

		public static ArrayList<Contact> convert(HashMap<String, String> contacts, HashMap<String, ContactVisibility> privacy)
		{
			ArrayList<Contact> result = new ArrayList<Profile.Contact>();

			for(CommunicationType type : CommunicationType.values())
			{
				if(contacts.containsKey(type.getKey()))
				{
					result.add(new Contact(type, contacts.get(type.getKey()), privacy.get(type.getKey())));
				}
			}

			return result;
		}
	}

	public static enum ContactVisibility
	{
		Invisible,
		Semivisible,
		Visible
	}
}

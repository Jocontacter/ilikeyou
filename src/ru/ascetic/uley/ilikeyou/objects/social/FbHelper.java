﻿package ru.ascetic.uley.ilikeyou.objects.social;

import java.net.URL;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.Profile.Contact;
import ru.ascetic.uley.ilikeyou.objects.Profile.ContactVisibility;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.Sex;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.facebook.model.GraphUser;

public class FbHelper
{
	private Context context;
	private GraphUser user;
	private Profile userProfile;

	public FbHelper(Context ctx)
	{
		this.context = ctx;
	}

	public FbHelper setUserLink(GraphUser user, Profile profile)
	{
		this.user = user;
		this.userProfile = profile;
		return this;
	}

	public boolean fillFbUserInfo(final List<String> fields)
	{
		boolean import_nick = fields.contains(context.getString(R.string.import_nick));
		boolean import_photo = fields.contains(context.getString(R.string.import_photo));
		boolean import_birthday = fields.contains(context.getString(R.string.import_birthday));

		if(import_photo)
		{
			Bitmap avatar = null;
			try
			{
				URL img_value = new URL("https" + "://graph.facebook.com/" + user.getId() + "/picture?type=large");

				avatar = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			}
			catch(Exception e)
			{
				Log.e("ily", "Exception", e);
				return false;
			}

			if(avatar != null)
			{
				userProfile.setImage(avatar);
			}
		}

		if(import_birthday)
			userProfile.birthday = Util.getMsFromDateString(user.getBirthday(), "MM/dd/yyyy");

		if(import_nick)
			userProfile.nick = user.getName();

		String sex = user.getProperty("gender").toString();
		sex = sex.substring(0, 1).toUpperCase(context.getResources().getConfiguration().locale) + sex.substring(1);
		userProfile.sex = Sex.valueOf(Sex.class, sex);

		userProfile.putContact(new Contact(CommunicationType.FACEBOOK, "www.facebook.com/" + user.getId(), ContactVisibility.Invisible));

		userProfile.saveAll();

		return true;
	}
}
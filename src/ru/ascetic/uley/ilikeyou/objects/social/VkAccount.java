﻿package ru.ascetic.uley.ilikeyou.objects.social;

import java.net.URL;
import java.util.List;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.objects.Profile.Contact;
import ru.ascetic.uley.ilikeyou.objects.Profile.ContactVisibility;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.CommunicationType;
import ru.ascetic.uley.ilikeyou.objects.SocialUser.Sex;
import ru.ascetic.uley.ilikeyou.objects.social.VKSDKListener.VKUserTokenRecipient;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

public class VkAccount implements VKUserTokenRecipient
{
	private Context context = null;

	private VKAccessToken vkAccessToken;

	private String appID = null;

	private VKAutorizationListener autorizationListener;

	private VKApiUserFull user;

	public interface ProfileLinkStatusListener
	{
		public void onSuccess(VkAccount user);
	}

	public interface VKAutorizationListener
	{
		public void onAutorize(boolean success);
	}

	public interface ProfileVKLinkStatusListener
	{
		public void onSuccess(VkAccount user);
	}

	/**
	 * Scope is set of required permissions for your application
	 * 
	 * @see <a href="https://vk.com/dev/permissions">vk.com api permissions documentation</a>
	 */
	private static final String[] sMyScope = new String[]
		{ VKScope.DIRECT, VKScope.FRIENDS, VKScope.PHOTOS, VKScope.NOHTTPS };

	public VkAccount(Context context, String api_id)
	{
		this.context = context;
		this.appID = api_id;
		restore();
	}

	/**
	 * @return the access_token
	 **/
	public String getAccessToken()
	{
		if(vkAccessToken == null)
			return null;
		return vkAccessToken.accessToken;
	}

	/**
	 * @return the user_id
	 **/
	public String getUserID()
	{
		if(vkAccessToken == null)
			return null;
		return vkAccessToken.userId;
	}

	public void autorize(boolean revoke, boolean forceOAuth, VKAutorizationListener autorizationListener)
	{
		this.autorizationListener = autorizationListener;

		VKSdk.initialize(new VKSDKListener(sMyScope, this), appID, vkAccessToken);

		if(!VKSdk.wakeUpSession())
		{
			VKSdk.authorize(sMyScope, true, false);
		}
	}

	private void restore()
	{
		vkAccessToken = VKAccessToken.tokenFromSharedPreferences(context, "vk_access_token");
	}

	/**
	 * Fills nick, birthday, sex, photo from vk account
	 * 
	 * @param profile
	 *            - wich account to fill
	 * @return true if success
	 **/
	public boolean linkProfile(final Profile profile, List<String> fields)
	{
		if(vkAccessToken == null)
			throw new IllegalStateException("access token is null!");

		if(user == null)
			throw new IllegalStateException("vk user is null!");

		try
		{
			boolean import_nick = fields.contains(context.getString(R.string.import_nick));
			boolean import_photo = fields.contains(context.getString(R.string.import_photo));
			boolean import_birthday = fields.contains(context.getString(R.string.import_birthday));

			if(import_photo)
			{
				URL url = new URL(user.photo_200);
				profile.setImage(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
				profile.saveImage();
			}

			if(import_birthday)
			{
				profile.birthday = Util.getMsFromDateString(user.bdate, "dd.MM.yyyy");
			}

			if(import_nick)
			{
				profile.nick = user.first_name + " " + user.last_name;
			}

			switch(user.sex)
			{
				case 0:
					profile.sex = Sex.Unknown;
					break;
				case 1:
					profile.sex = Sex.Female;
					break;
				case 2:
					profile.sex = Sex.Male;
					break;
			}

			profile.saveGeneral();

			if(user.skype != null && !profile.getContacts().containsKey(CommunicationType.SKYPE.getKey()))
				profile.putContact(new Contact(CommunicationType.SKYPE, user.skype, ContactVisibility.Invisible));

			profile.putContact(new Contact(CommunicationType.VKONTAKTE, "http://vk.com/" + user.fields.getString("domain"),
					ContactVisibility.Invisible));

			profile.saveContacts();

			return true;
		}
		catch(Exception e)
		{
			Log.e("ily", "vk req exeption", e);
		}

		return false;
	}

	public void logOut()
	{
		VKAccessToken.removeTokenAtKey(context, "vk_access_token");
	}

	@Override
	public void onAcceptVKAccessToken(VKAccessToken token)
	{
		this.vkAccessToken = token;
		this.vkAccessToken.saveTokenToSharedPreferences(context, "vk_access_token");

		VKRequest request = VKApi.users().get(
				VKParameters.from(VKApiConst.FIELDS, "first_name,last_name,nickname,screen_name,domain,photo_200,sex,bdate,connections,contacts",
						VKApiConst.NAME_CASE, "nom"));

		request.executeWithListener(new VKRequestListener()
		{
			@Override
			public void onComplete(VKResponse response)
			{
				VKList<VKApiUserFull> list = (VKList<VKApiUserFull>) response.parsedModel;
				user = list.get(0);

				autorizationListener.onAutorize(true);
			}

			@Override
			public void onError(VKError error)
			{
				autorizationListener.onAutorize(false);
			}

			@Override
			public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts)
			{
				Log.d("VkAccount", String.format("VKRequest 'users.get' -> attempt %s/%s failed", attemptNumber, totalAttempts));
			}
		});
	}

	@Override
	public void onAcceptTokenFailed()
	{
		autorizationListener.onAutorize(false);
	}
}
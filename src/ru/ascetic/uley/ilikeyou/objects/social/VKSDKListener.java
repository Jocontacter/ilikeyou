package ru.ascetic.uley.ilikeyou.objects.social;

import android.app.AlertDialog;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;

public class VKSDKListener extends com.vk.sdk.VKSdkListener
{
	private String[] sMyScope;
	private VKUserTokenRecipient tokenRecipient;

	public interface VKUserTokenRecipient
	{
		public void onAcceptVKAccessToken(VKAccessToken token);

		public void onAcceptTokenFailed();
	}

	public VKSDKListener(String[] scope, VKUserTokenRecipient tokenRecipient)
	{
		this.sMyScope = scope;
		this.tokenRecipient = tokenRecipient;
	}

	@Override
	public void onAcceptUserToken(VKAccessToken token)
	{
		tokenRecipient.onAcceptVKAccessToken(token);
	}

	@Override
	public void onReceiveNewToken(VKAccessToken newToken)
	{
		tokenRecipient.onAcceptVKAccessToken(newToken);
	}

	@Override
	public void onRenewAccessToken(VKAccessToken token)
	{
		tokenRecipient.onAcceptVKAccessToken(token);
	}

	@Override
	public void onCaptchaError(VKError captchaError)
	{
		new VKCaptchaDialog(captchaError).show();
	}

	@Override
	public void onTokenExpired(VKAccessToken expiredToken)
	{
		VKSdk.authorize(sMyScope);
	}

	@Override
	public void onAccessDenied(VKError authorizationError)
	{
		new AlertDialog.Builder(VKUIHelper.getTopActivity()).setMessage(authorizationError.toString()).show();
		tokenRecipient.onAcceptTokenFailed();
	}

}

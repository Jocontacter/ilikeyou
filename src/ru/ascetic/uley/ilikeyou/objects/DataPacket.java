﻿package ru.ascetic.uley.ilikeyou.objects;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.data.dao.MessageDAO;
import ru.ascetic.uley.ilikeyou.data.dao.SocialUserDAO;
import ru.ascetic.uley.ilikeyou.objects.Message.Direction;
import android.util.Log;

public class DataPacket
{
	private String gid = null;
	private String userContent = null;
	private String digest = null;
	private boolean likeTo = false;
	private boolean likeFrom = false;
	private List<Message> messages = null;

	private int[] remoteMessageIds = null;

	/**
	 * @param userContent
	 *            - serialized user if needed. If not needed - pass null.
	 * @param likeTo
	 *            - have a like for remote user or not
	 * @param message
	 *            - message to remote user or null
	 **/
	private DataPacket(String gid, String userContent, boolean likeTo, boolean likeFrom, String digest, List<Message> messages)
	{
		this.gid = gid;
		this.userContent = userContent;
		this.digest = digest;
		this.likeTo = likeTo;
		this.likeFrom = likeFrom;
		this.messages = messages;
	}

	public static DataPacket parse(String jsonContent)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(jsonContent);
			String gid = jsonObject.getString("gid");
			String personContent = jsonObject.optString("person_data", null);
			String digest = jsonObject.optString("digest", null);
			boolean likeTo = jsonObject.optBoolean("like_to");
			boolean likeFrom = jsonObject.optBoolean("like_from");
			JSONArray jsonMessages = jsonObject.optJSONArray("messages");
			JSONArray jsonMessageIDs = jsonObject.optJSONArray("message_ids");

			List<Message> toMessages = new ArrayList<Message>();

			MessageDAO dao;

			int[] remoteIds;

			try
			{
				dao = HelperFactory.getHelper().getMessageDAO();
				remoteIds = dao.getFromRemoteIds(gid);
			}
			catch(SQLException e1)
			{
				Log.e("ily", "Exception", e1);
				throw new RuntimeException(e1);
			}

			Arrays.sort(remoteIds);

			for(int i = 0; i < jsonMessages.length(); i++)
			{
				JSONObject message = jsonMessages.getJSONObject(i);

				int remoteId = message.getInt("id");

				// Если у нас такого сообщения еще нет - добавляем
				if(Arrays.binarySearch(remoteIds, remoteId) < 0)
				{
					Message m = new Message();
					m.setDirection(Direction.To);
					m.setMessage(message.getString("message"));
					m.setRemoteMessageId(remoteId);
					m.setRemoteGid(gid);
					m.setDelivered(System.currentTimeMillis());

					try
					{
						dao.create(m);
						toMessages.add(m);
					}
					catch(SQLException e)
					{
						Log.e("ily", "Exception", e);
					}
				}
			}

			for(int i = 0; i < jsonMessageIDs.length(); i++)
			{
				int messageID = jsonMessageIDs.getInt(i);

				dao.updateDeliveredTime(messageID);
			}

			return new DataPacket(gid, personContent, likeTo, likeFrom, digest, toMessages);
		}
		catch(JSONException e)
		{
			Log.e("ily", "Exception", e);
			throw new RuntimeException(e);
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param localprofile
	 *            - профиль пользователя
	 * @param remoteUserGid
	 *            - gid пользователя другого устройства
	 * @param needsToUpdate
	 *            - требуется ли насильная передача данных нашего профиля(remote device сообщил, что не имеет актуальных
	 *            данных нашего профайла)
	 * @return
	 **/
	public static DataPacket create(Profile localprofile, String remoteUserGid, boolean needsToUpdate)
	{
		String digest = null;
		boolean likeTo = false;
		boolean likeFrom = false;
		String userContent = null;
		List<Message> toMessages = null;
		int[] remoteMessIds = null;
		SocialUser remoteUser = null;
		try
		{
			SocialUserDAO userDao = HelperFactory.getHelper().getSocialUserDAO();

			MessageDAO messageDao = HelperFactory.getHelper().getMessageDAO();

			remoteMessIds = messageDao.getFromRemoteIds(remoteUserGid);

			remoteUser = userDao.getUserByGid(remoteUserGid);
			boolean isNull = remoteUser == null;

			if(!isNull)
			{
				likeTo = remoteUser.likeTo;
				likeFrom = remoteUser.likeFrom;
			}

			localprofile.updateDigest();
			digest = localprofile.getDigest();

			// if remote device does lost our profile data, needsToUpdate will be true and userContent must be filled
			// if we are not have remote profile data, suppose this is unknown user for us and userContent must be
			// filled
			// if local digest does not equals stored last sent to remote device profile data digest - userContent must
			// be filled
			if(needsToUpdate || isNull || !digest.equals(remoteUser.local_digest))
			{
				if(!isNull)
				{
					// There digest update only! Assign new value occurs while creation - in
					// UleyDataProvider.receiveData()
					remoteUser.local_digest = digest;
					userDao.updateLightweightFields(remoteUser);
				}

				userContent = localprofile.serializeForTransfer(likeTo);
			}

			toMessages = messageDao.getUnsent(remoteUserGid);

			long when = System.currentTimeMillis();
			for(Message message : toMessages)
			{
				message.setSent(when++);
				messageDao.update(message);
			}
		}
		catch(SQLException e)
		{
			Log.e("ily", "Exception", e);
			throw new RuntimeException(e);
		}

		DataPacket result = new DataPacket(localprofile.gid, userContent, likeTo, likeFrom, digest, toMessages);
		result.remoteMessageIds = remoteMessIds;

		return result;
	}

	/** create a data packet for sending to remote device **/
	public String createPacket()
	{
		JSONObject packet = new JSONObject();
		try
		{
			packet.put("gid", gid);
			packet.put("like_to", likeTo);
			packet.put("like_from", likeFrom);
			packet.put("digest", digest);

			if(userContent != null)
				packet.put("person_data", userContent);

			packet.put("message_ids", new JSONArray());
			packet.put("messages", new JSONArray());

			for(int i = 0; i < remoteMessageIds.length; i++)
			{
				packet.accumulate("message_ids", remoteMessageIds[i]);
			}

			if(messages.size() > 0)
			{
				JSONObject jsonMessage;
				for(Message message : messages)
				{
					jsonMessage = new JSONObject();
					jsonMessage.put("id", message.getId());
					jsonMessage.put("message", message.getMessage());

					packet.accumulate("messages", jsonMessage);
				}
			}
		}
		catch(JSONException e)
		{
			return "error";
		}

		return packet.toString();
	}

	/** if remote user not changed from last connection - returns null **/
	public SocialUser getRemoteUser()
	{
		SocialUser user = null;
		if(userContent != null)
		{
			user = new SocialUser();
			user.deserialize(userContent);
		}

		return user;
	}

	/** remote user gid **/
	public String getGid()
	{
		return gid;
	}

	/**
	 * Принимающей стороне следует рассматривать этот лайк как входящий для профиля принимающей стороны
	 **/
	public boolean getLikeTo()
	{
		return likeTo;
	}

	/**
	 * Принимающей стороне следует рассматривать этот лайк как исходящий от профиля принимающей стороны
	 **/
	public boolean getLikeFrom()
	{
		return likeFrom;
	}

	/**
	 * @return the digest
	 **/
	public String getDigest()
	{
		return digest;
	}

	/**
	 * @return the messages
	 **/
	public List<Message> getMessages()
	{
		return messages;
	}
}

﻿package ru.ascetic.uley.ilikeyou.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Connection class - stores all generic information about connections between two users.
 * 
 * @author Evgeniy
 **/
@DatabaseTable(tableName="connections")
public class Connection
{
	
	/** remote user gid **/
	@DatabaseField(id=true)
	public String remote_gid;
	
	/** local user digest(last actual version for remote user) **/
	@DatabaseField(canBeNull=false)
	public String local_digest;
	
	/** whether a like to remote user or not **/
	@DatabaseField
	public boolean likeTo = false;
	
	/** whether a like from remote user or not **/
	@DatabaseField
	public boolean likeFrom = false;
	
	/** last connection time **/
	@DatabaseField(canBeNull=false)
	public long last_connection_time = 0;
	
	
	/** counter of connections - readonly! increments in db. **/
	@DatabaseField(canBeNull=false, version=true)
	private int connection_counter = 0;
	
	
	/**
	 * Empty constructor for liteOrm
	 **/
	public Connection()
	{
	}
	
	/**
	 * New connection. Contains remote user gid, local user digest(last actual version for remote user), last connection time in ms and counter of connections
	 * 
	 * @param remote_gid remote user gid
	 * @param local_digest local user digest(last actual version for remote user)
	 * @param last_connection_time - last connection time
	 * @param connection_counter counter of connections
	 **/
	public Connection(String remote_gid, String local_digest, long last_connection_time)
	{
		this.remote_gid = remote_gid;
		this.local_digest = local_digest;
		this.last_connection_time = last_connection_time;
	}
	
	public int getConnectionCounter()
	{
		return connection_counter;
	}
}

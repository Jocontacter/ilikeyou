﻿package ru.ascetic.uley.ilikeyou.objects;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.ascetic.uley.ilikeyou.R;
import ru.ascetic.uley.ilikeyou.Util;
import android.graphics.Bitmap;
import android.text.InputType;
import android.util.Base64;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "persons")
public class SocialUser
{
	@DatabaseField(id = true)
	public String gid = null;

	/** Not for use! Use getDigest() instead! **/
	@DatabaseField(useGetSet = true)
	private String digest = null;

	/** User nick **/
	@DatabaseField
	public String nick = "";

	/** User status **/
	@DatabaseField
	public String status = "";

	@DatabaseField(unknownEnumName = "Unknown")
	public Sex sex = Sex.Unknown;

	/** date of born **/
	@DatabaseField
	public long birthday = 0;

	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	public byte[] imageBytes;

	/**
	 * map kind of: "vk"=>"vk_link" "ph"=>"phone number" and so on..
	 **/
	@DatabaseField(dataType = DataType.SERIALIZABLE)
	protected HashMap<String, String> contacts = new HashMap<String, String>();

	/** timestamp of last connection **/
	@DatabaseField(canBeNull = false)
	public long lastConnection = 0;

	/** local user digest(last actual version for remote user) **/
	@DatabaseField(canBeNull = false, dataType = DataType.STRING_BYTES)
	public String local_digest = null;

	/** whether a like to remote user or not **/
	@DatabaseField(canBeNull = false)
	public boolean likeTo = false;

	/** whether a like from remote user or not **/
	@DatabaseField(canBeNull = false)
	public boolean likeFrom = false;

	/** counter of connections - readonly! increments in db. **/
	@DatabaseField(canBeNull = false)
	// , version=true
	private int connection_counter = 0;

	/**
	 * just useful field for in-memory use
	 * 
	 * @remark впервые понадобилось для передачи вместе с пользователем количества новых сообщений для него в Adapter
	 **/
	public Object tag = null;

	/** main avatar TODO: add image collection **/
	public Bitmap getImage()
	{
		if(imageBytes == null)
			return null;

		return Util.bytesToBitmap(imageBytes);
	}

	public void setImage(Bitmap image)
	{
		imageBytes = Util.bitmapToBytes(image);
	}

	public UUID getGid()
	{
		return UUID.fromString(gid);
	}

	public String getDigest()
	{
		if(digest == null)
			updateDigest();

		return digest;
	}

	protected void updateDigest()
	{
		this.updateDigest("");
	}

	protected void updateDigest(String customPart)
	{
		try
		{
			// сомнения насчет String.valueOf(likeTo) - для профайла не имеет значения...
			String text = gid + (imageBytes == null ? 0 : imageBytes.length) + String.valueOf(contacts.size()) + String.valueOf(contacts.hashCode())
					+ String.valueOf(likeTo) + nick + birthday + status + sex.value + customPart;

			MessageDigest mdigest = MessageDigest.getInstance("MD5");
			digest = new String(mdigest.digest(text.getBytes("UTF-8")));
		}
		catch(NoSuchAlgorithmException ex)
		{
			throw new RuntimeException("No MD5 implementation? Really?");
		}
		catch(UnsupportedEncodingException ex)
		{
			throw new RuntimeException("No UTF-8 encoding? Really?");
		}
	}

	public void setDigest(String digest)
	{
		this.digest = digest;
	}

	/**
	 * map kind of: "vk"=>"vk_link" "ph"=>"phone number" and so on..
	 **/
	public HashMap<String, String> getContacts()
	{
		return contacts;
	}

	public int getConnectionCounter()
	{
		return connection_counter;
	}

	public SocialUser()
	{
	}

	public JSONObject deserialize(String serializedUser)
	{
		JSONObject obj = null;

		try
		{
			obj = new JSONObject(serializedUser);
			nick = obj.optString("nick");
			birthday = obj.optLong("birthday");
			sex = Sex.valueOf(obj.optString("sex", Sex.Unknown.name()));

			status = obj.optString("status");

			gid = obj.optString("gid");
			digest = obj.optString("digest", null);

			JSONObject jsonContacts = obj.optJSONObject("contacts");

			if(jsonContacts != null)
			{
				JSONArray keysJson = jsonContacts.names();

				if(keysJson != null)
				{
					for(int i = 0; i < keysJson.length(); i++)
					{
						String key = keysJson.getString(i);
						contacts.put(key, jsonContacts.getString(key));
					}
				}
			}

			String imageString = obj.optString("image");

			if(imageString != null && imageString.length() > 0)
				imageBytes = Base64.decode(imageString, Base64.DEFAULT);
		}
		catch(JSONException e)
		{
			throw new RuntimeException(e);
		}
		return obj;
	}

	public long getSinceMs()
	{
		return System.currentTimeMillis() - lastConnection;
	}

	public String getBirthdayString(String template, Locale locale)
	{
		DateFormat formatter = new SimpleDateFormat(template, locale);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(birthday);
		return formatter.format(calendar.getTime());
	}

	public int getAge()
	{
		return getAge(birthday);
	}

	public static int getAge(long birthdateInMs)
	{
		GregorianCalendar cal = new GregorianCalendar();
		int y, m, d, a;

		y = cal.get(Calendar.YEAR);
		m = cal.get(Calendar.MONTH);
		d = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTimeInMillis(birthdateInMs);

		a = y - cal.get(Calendar.YEAR);
		if((m < cal.get(Calendar.MONTH)) || ((m == cal.get(Calendar.MONTH)) && (d < cal.get(Calendar.DAY_OF_MONTH))))
		{
			--a;
		}
		if(a < 0)
			return 0;
		return a;
	}

	/** Пол пользователя(1 - женский, 2 - мужской, 0 - без указания пола.) **/
	public enum Sex
	{
		Unknown(0),
		Female(1),
		Male(2);

		private int value;

		Sex(int sex_value)
		{
			value = sex_value;
		}

		public int getValue()
		{
			return value;
		}
	}

	public static enum CommunicationType
	{
		/** vkontakte **/
		VKONTAKTE("vk", R.drawable.contacts_vkontakte, R.string.vk, InputType.TYPE_CLASS_TEXT),
		/** facebook **/
		FACEBOOK("fb", R.drawable.contacts_facebook, R.string.fb, InputType.TYPE_CLASS_TEXT),
		/** twitter **/
		TWITTER("tw", R.drawable.contacts_twitter, R.string.tw, InputType.TYPE_CLASS_TEXT),
		/** skype **/
		SKYPE("sk", R.drawable.contacts_skype, R.string.sk, InputType.TYPE_CLASS_TEXT),
		/** phone **/
		PHONE("ph", R.drawable.contacts_phone, R.string.ph, InputType.TYPE_CLASS_PHONE),
		/** phone **/
		EMAIL("em", R.drawable.contacts_email, R.string.em, InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

		private String key;
		private int drawableResource;
		private int stringResource;
		private int inputType;

		private CommunicationType(String key, int drawableResource, int stringResource, int inputType)
		{
			this.key = key;
			this.drawableResource = drawableResource;
			this.stringResource = stringResource;
			this.inputType = inputType;
		}

		public static CommunicationType getByKey(String key)
		{
			for(CommunicationType cnt : CommunicationType.values())
			{
				if(key.equals(cnt.getKey()))
					return cnt;
			}

			return null;
		}

		public String getKey()
		{
			return key;
		}

		public int getDrawableResource()
		{
			return drawableResource;
		}

		public int getStringResource()
		{
			return stringResource;
		}

		public int getInputType()
		{
			return inputType;
		}
	}
}

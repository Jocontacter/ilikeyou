﻿package ru.ascetic.uley.ilikeyou.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="messages")
public class Message
{
	@DatabaseField(generatedId=true)
	private int id = 0;
	
	@DatabaseField
	private int remoteMessageId = 0;
	
	/** remote user gid **/
	@DatabaseField
	public String remote_gid;
	
	@DatabaseField
	private String message;
	
	@DatabaseField
	private long sent;
	
	@DatabaseField
	private long delivered;
	
	@DatabaseField
	private Direction direction;

	public Message()
	{
	}
	
	public String getRemoteGid()
	{
		return remote_gid;
	}

	public void setRemoteGid(String remote_gid)
	{
		this.remote_gid = remote_gid;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * For outgoing messages - send time
	 * Default equals 0
	 **/
	public long getSent()
	{
		return sent;
	}

	/**
	 * For outgoing messages - send time
	 * Default equals 0
	 **/
	public void setSent(long when)
	{
		this.sent = when;
	}

	/**
	 * For income - delivery time
	 * Default equals 0
	 **/
	public long getDelivered()
	{
		return delivered;
	}

	/**
	 * For income - delivery time
	 * Default equals 0
	 **/
	public void setDelivered(long delivered)
	{
		this.delivered = delivered;
	}

	public Direction getDirection()
	{
		return direction;
	}

	public void setDirection(Direction direction)
	{
		this.direction = direction;
	}

	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}

	public int getRemoteMessageId()
	{
		return remoteMessageId;
	}

	public void setRemoteMessageId(int remoteMessageId)
	{
		this.remoteMessageId = remoteMessageId;
	}

	public static enum Direction
	{
		/** To remote user**/
		From,
		/** From remote user **/
		To
	}
}

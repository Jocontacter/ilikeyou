﻿package ru.ascetic.uley.ilikeyou;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;

public class Util
{

	public static byte[] bitmapToBytes(Bitmap bmp)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		return stream.toByteArray();
	}

	public static String bitmapToBase64(Bitmap bmp)
	{
		return Base64.encodeToString(bitmapToBytes(bmp), Base64.DEFAULT);
	}

	public static Bitmap base64ToBitmap(String base64String)
	{
		byte[] barr = Base64.decode(base64String, Base64.DEFAULT);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		return BitmapFactory.decodeByteArray(barr, 0, barr.length, options);
	}

	public static Bitmap bytesToBitmap(byte[] bytes)
	{
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
	}

	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size = 1024;
		try
		{
			byte[] bytes = new byte[buffer_size];
			for(;;)
			{
				int count = is.read(bytes, 0, buffer_size);
				if(count == -1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex)
		{
		}
	}

	/**
	 * @param date
	 *            - in format: dd.mm.yyyy
	 * @return
	 **/
	public static long getMsFromDateString(String date, String format)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);

		Date oldDate;
		try
		{
			oldDate = formatter.parse(date);
		}
		catch(ParseException e)
		{
			return 0;
		}

		return oldDate.getTime();
	}

	/**
	 * Пропорциональный ресайз с ограничением по длине и/или ширине
	 **/
	public static Bitmap getResizedBitmap(Bitmap bm, int maxWidth, int maxHeight)
	{
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scale = 1;
		int xD = width - maxWidth;
		int yD = height - maxHeight;

		if(xD > yD && xD > 0)
		{
			scale = (float) maxWidth / (float) width;
		}
		else if(yD > xD && yD > 0)
		{
			scale = maxHeight / height;
		}
		else
			return bm;

		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scale, scale);
		// "RECREATE" THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createScaledBitmap(bm, (int) (width * scale), (int) (height * scale), true);
		// Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
		bm.recycle();

		return resizedBitmap;
	}

	/**
	 * Загружает bmp из файла c конфигурацией Bitmap.Config.ARGB_8888
	 * 
	 **/
	public static Bitmap loadBitmap(String path)
	{
		return loadBitmap(path, null);
	}

	/**
	 * Загружает bmp из файла
	 * 
	 * @param options
	 *            can be null
	 **/
	public static Bitmap loadBitmap(String path, BitmapFactory.Options options)
	{
		if(options == null)
		{
			options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		}
		return BitmapFactory.decodeFile(path, options);
	}

	public static String getAgeString(Context context, int age)
	{
		return age + " " + getSuffix(context, age);
	}

	public static int getNearColor(long elapsed, int basecolor)
	{
		long max_since = LikeYouApplication.SINCE_MAX;

		int alpha = (int) (((float) 255 / max_since) * Math.max((max_since - elapsed), 0));

		return ((alpha << 24) | 0x01ffffff) & basecolor;
	}

	private static String getSuffix(Context context, int age)
	{
		int lastDigit = age % 10;

		if(age > 4 && age < 21)
			return context.getString(R.string.suffix_let);
		if(lastDigit == 1)
			return context.getString(R.string.suffix_god);
		if(lastDigit > 1 && lastDigit < 5)
			return context.getString(R.string.suffix_goda);
		return context.getString(R.string.suffix_let);
	}
}

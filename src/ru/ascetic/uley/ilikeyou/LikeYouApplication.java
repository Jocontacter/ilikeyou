﻿package ru.ascetic.uley.ilikeyou;

import ru.ascetic.uley.api.UleyClientBroadcastReceiver;
import ru.ascetic.uley.ilikeyou.data.HelperFactory;
import ru.ascetic.uley.ilikeyou.objects.Profile;
import ru.ascetic.uley.ilikeyou.sync.UleyDataProvider;
import android.app.Application;
import android.app.NotificationManager;

public class LikeYouApplication extends Application
{
	public static final boolean IS_DEBUG_MODE = true;

	public static final int SINCE_MAX = 1000 * 60 * 2;

	@Override
	public void onCreate()
	{
		super.onCreate();
		HelperFactory.setHelper(getApplicationContext());
		ru.ascetic.uley.data.HelperFactory.setHelper(HelperFactory.getHelper());

		Profile.getInstance(this);

		// обновляем на сервисе инфомацию о приложении:
		UleyClientBroadcastReceiver.sendClientInfo(this, new UleyDataProvider());

		if(LikeYouApplication.IS_DEBUG_MODE)
		{
			DebugUtil.logFbKeyhash(this);
			DebugUtil.logVkFingerprint(this);
		}
	}

	@Override
	public void onTerminate()
	{
		HelperFactory.releaseHelper();
		ru.ascetic.uley.data.HelperFactory.releaseHelper();
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.cancelAll();
		super.onTerminate();
	}
}

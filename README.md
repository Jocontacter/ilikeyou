# README #

**iLikeYou**(In touch) - требует для своей работы:

* сервис [Uley](https://bitbucket.org/Jocontacter/uley/wiki/Home)(включается в проект как библиотека)
* библиотека-хелпер [UleyAPI](https://bitbucket.org/Jocontacter/uleyapi)
* проект [официального API для Android от *Facebook*](https://developers.facebook.com/docs/android/getting-started)
* проект [официального API для Android от *VKontakte*](https://vk.com/dev/android_sdk)
* ormlite: ormlite-android-4.47.jar + ormlite-core-4.47.jar
* библиотека [viewbadger](https://github.com/jgilfelt/android-viewbadger)
* библиотека [NewQuickAction](https://github.com/lorensiuswlt/NewQuickAction)

### Страницы ###

* [Квесты](wiki/Квесты)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact